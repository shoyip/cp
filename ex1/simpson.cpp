#include <iostream>
#include <cmath>

int main() {
	double ljpi (double a, double b, int np, double e);
	//double result = ljpi(1.02013, 1.43884, 1000, -0.4);
	double result = ljpi(1.00908, 1.63272, 1000, -0.2);
	std::cout << result << std::endl;
	return 0;
}

double ljp (double x) {
	return 4.0*(pow(x, -12) - pow(x, -6));
}

double eljp (double x, double e) {
	return e - 4.0*(pow(x, -12) - pow(x, -6));
}

// definition of integrand function
double f1 (double x, double e) {
	return sqrt(fabs(e - 4.0*pow(x, -12) + 4*pow(x, -6)));
}

double ljpi (double a, double b, int np, double e) {
	double h, oddsum=0.0, evensum=0.0, sum=0.0;
	double ljp (double x);
	double f1 (double x, double e);
	h = (double) (b-a)/np;
	for (int i=1; i<=np/2; i++) {
		sum += f1(a+(2*i-2)*h, e) + 4.0*f1(a+(2*i-1)*h, e) + f1(a+(2*i)*h, e);
	}
	return sum*h/3.0;
}
