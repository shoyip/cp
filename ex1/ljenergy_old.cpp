#include <iostream>
#include <cmath>

const double pi = 3.14159;

int main() {
	const double gamma = 21.7; // H2
//	const double gamma = 24.8; // HO
//	const double gamma = 150.0; // O2
	const int maxn = 100;
	void search(int n, double e1);
	for (int n=0; n<=0; n++) {
		double e1 = (n+0.5)*pi*gamma;
		search(n, e1);
	}

	return 0;
}

void search(int n, double e1) {
	double ljpi (double a, double b, int n, double e);
	double s, x1, x2;
	double estep = 1e-3;
	int maxstep = 1000;
	double e0 = -1+estep; // guess energy
//	double e1; // expected energy
	const double et = 1e-6; // tolerance on energy value
	while ((sqrt((s-e1)*(s-e1))>et) && e0<0) {
		x1 = pow((1-sqrt(2*(1+e0)))/2, 6);
		x2 = pow((1+sqrt(2*(1+e0)))/2, 6);
		double s = ljpi(x1, x2, 1000, e0);
		std::cout << "x1=" << x1 << " x2=" << x2 << " expE=" << e1 << " s=" << s << "\n";
//		std::cout << x1 << "," << x2 << "," << e0 << std::endl;
		e0 += estep;
	}
//	std::cout << "Level found for n=" << n << " with energy En=" << e0 << std::endl;
}

// void action(double E, double xin, double xout, double S)
// {
// }

// definition of lennard jones potential
double ljp (double x)
{
	return 4*(pow(x, -12) - pow(x, -6));
}

// definition of the integrand function
double f1 (double x, double e) {
	return sqrt(abs(e - 4*pow(x, -12) + 4*pow(x, -6)));
}

// definition of the integral
double ljpi (double a, double b, int np, double e) {
	double h, x[np+1], sum=0.0;
	double ljp (double x);
	double f1 (double x, double e);
	int i;
	x[0]=a;
	h = (b-a)/np;
	for (i=1; i<=np; i++) {
		x[i] = a+i*h;
//		std::cout << x[i] << std::endl;
	}
	for (i=1; i<=np/2; i++) {
		sum = f1(x[2*i-2], e) + 4*f1(x[2*i-1], e) + f1(x[2*i], e);
//		std::cout << sum << std::endl;
	}
	return sum*h/3.0;
}
