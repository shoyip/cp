#include <iostream>
#include <cmath>

int main () {
	double bisection (double a, double b, double e);
	double result = bisection(pow(2.0, 1.0/6.0), 100, -0.4);
	std::cout << result << std::endl;
	return 0;
}

double ljp (double x) {
	return 4*(pow(x, -12) - pow(x, -6));
}

double eljp (double x, double e) {
	return e - 4.0*(pow(x, -12) - pow(x, -6));
}


double bisection (double a, double b, double e) {
	double c = a;
	double eljp (double x, double e);
	double const epsilon = 0.0001;
	while ((b-a) >= epsilon) {
		c = (a+b)/2;
		std::cout << c << std::endl;
		if (eljp(c, e) == 0.0)
			break;
		else if (eljp(c, e) * eljp(a, e) <0)
			b=c;
		else
			a=c;
	}
	std::cout << c << std::endl;
	return c;
}
