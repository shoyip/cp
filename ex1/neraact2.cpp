#include<iostream>
#include<cmath>
#include <iomanip>
#define MAXIT 60
#define ET 1e-10
#define XT 0.000001
#define NP 10000
#define PM pow(2.0, 1.0/6.0)
#define PI 3.14159265359
#define GAMMA 24.8
#define LOG(x) std::cout << x << std::endl



int main(){
double nr (double guess, double se);
double e = -1;
int nmax = GAMMA/PI*0.841309-.5;

for(int n = 0; n<= nmax; n++){
  double se = (n+.5)*PI/GAMMA;
  nr(e, se);
  LOG(e);
}

return 0;
}


double vx (double x){
  return 4.0*(pow(x, -12) - pow(x, -6));
}

double fx (double x, double e){
    return sqrt(fabs(e - vx(x)));
}

double integral (double xz1, double xz2, double st, double e){
  double vx (double x);
  double fx (double x,double e);
  double h;
  double in = 0;
  h = (xz2 - xz1)/st;
  for (int i=0; i<=st-1; i++){

    in = in + (fx(xz1+i*h, e) + fx( xz1+h*(i+1) , e))*h/2;
  }
return in;
}

double nr ( double guess, double se) {
	double vx (double x);
	double fx (double x, double e);
  double integral (double xz1, double xz2, double st, double e);
	double k = 1e-7;
  double h, sum=0.0, dx, np=NP;
  double e, x1, x2, e1, e2, em1, em2, fe0, fe1, fe2, fem1, fem2, f1e0, newguess;
  double est = 1e-5;
  double max = pow(est, -1);

    for(int j = 1; j = max; j++ ){
      double guess = e + j*est;


      x1 = PM;
      dx = .1;
      while (dx > ET) {
        x1 -= dx;
      //	pot(x1) >= e ? (x1+=dx, dx/=2) : continue;
        if (vx(x1)>=e) {
          x1 += dx;
          dx /= 2;
        } else {
          continue;
        }
      }

      x2 = PM;
      dx = .1;
      while (dx > ET) {
        x2 += dx;
      //	pot(x2) >= e ? (x1-=dx, dx/=2) : continue;
        if (vx(x2)>=e) {
          x2 -= dx;
          dx /= 2;
        } else {
          continue;
        }
      }


      for(int i = 0; i <101; i++ ){

      e1 = guess + k;
    	e2 = guess + 2*k;
    	em1 = guess - k;
    	em2 = guess - 2*k;

    	fe0 = integral(x1, x2, 1000, guess) - se;
    	fe1 = integral(x1, x2, 1000, e1) - se;
    	fe2 = integral(x1, x2, 1000, e2) - se;
    	fem1 = integral(x1, x2, 1000, em1) - se;
    	fem2 = integral(x1, x2, 1000, em2) - se;

    	f1e0 = (-fe2 + 8*fe1 - 8*fem1 + fem2)/(12*h);

    	newguess = guess - fe0/f1e0;
    	guess = newguess;
	}


}
e = newguess;

}
