#include <iostream>
#include <cmath>
#include <iomanip>
#define MAXIT 60
#define ET 1e-10
#define XT 0.000001
#define NP 10000
#define PM pow(2.0, 1.0/6.0)
#define PI 3.14159265359
#define GAMMA 24.8
#define LOG(x) std::cout << x << std::endl


int main(){
double  e = -1;
double est = 1e-5;
double max = pow(est, -1);

double nr(double n, double e);

int nmax = GAMMA/PI*0.841309-.5;

for (int n = 0; n<=nmax; n++){
  double guess = e;




double  E =  nr(n, e );
    LOG(E);

}
return 0;
}

double nr(double n, double e){
double f (int n, double e, double &x1, double &x2);
double e1, e2, em1, em2, fe0, fe1, fe2, fem1, fem2, f1e0, newguess;
double k = 1e-7;
double ec = 0;


for(int i = 0; i = 100; i++){


e1 = guess + k;
e2 = guess + 2*k;
em1 = guess - k;
em2 = guess - 2*k;

fe0 = f(n, guess, x1, x2);
fe1 = f(n, e1, x1, x2);
fe2 = f(n, e2, x1, x2)
fem1 = f(n, em1, x1, x2);
fem2 = f(n, em2, x1, x2);

f1e0 = (-fe2 + 8*fe1 - 8*fem1 + fem2)/(12*k);

newguess = guess - fe0/f1e0;
guess = newguess;
e = newguess;
}

return newguess;
}


double f (int n, double e, double &x1, double &x2){
	double action (double &x1, double &x2, double e);
	return action(x1, x2, guess) - (n+.5)*PI/GAMMA;
}

double action (double &x1, double &x2, double e) {
	double h, sum=0.0, dx, np=NP;
	double pot (double x);
  e = guess;
	x1 = PM;
	dx = .1;
	while (dx > ET) {
		x1 -= dx;
	//	pot(x1) >= e ? (x1+=dx, dx/=2) : continue;
		if (pot(x1)>=e) {
			x1 += dx;
			dx /= 2;
		} else {
			continue;
		}
	}

	x2 = PM;
	dx = .1;
	while (dx > ET) {
		x2 += dx;
	//	pot(x2) >= e ? (x1-=dx, dx/=2) : continue;
		if (pot(x2)>=e) {
			x2 -= dx;
			dx /= 2;
		} else {
			continue;
		}
	}

	h = (x2-x1)/np;
	for (int i=1; i<=np/2; i++) {
		sum += sqrt(fabs(guess-pot(x1+(2*i-2)*h))) + 4.0*sqrt(fabs(guess-pot(x1+(2*i-1)*h))) + sqrt(fabs(guess-pot(x1+(2*i)*h)));
	}
	//for (int i=1; i<=np-1; i++) (i%2==0) ? (sum += 2.0*sqrt(e-pot(x1+i*h))) : (sum += 4.0*sqrt(e-pot(x1+i*h)));
	//sum = h/3.0 * (sqrt(e-pot(x1)) + sqrt(e-pot(x2)) + sum);
	return sum*h/3.0;
}

double pot (double x) {
	return 4.0 * (pow(x, -12.0) - pow(x, -6.0));
}
