#include <iostream>
#include <cmath>

double e = -0.4;

int main() {
	double eljp (double x);
	double bm (double (*func)(double), double a, double b, double xacc);
	double zero = bm(eljp, pow(2.0,1/6), 200, 1e-3);
	std::cout << zero << std::endl;
}

double bm (double (*func)(double), double a, double b, double xacc) {
	int j, jmax=100;
	double dx, f, fmid, xmid, rtb;

	f = (*func)(a);
	fmid = (*func)(b);
	if(f*fmid>=0.0) {
		std::cout << "ERROR: root in function must be between a and b." << std::endl;
		std::cout << f << " " << fmid << std::endl;
		exit(1);
	}
	rtb = f < 0.0 ? (dx = b-a, a) : (dx = a-b, b);
	for (j=0; j<= jmax; j++) {
		fmid = (*func)(xmid = rtb + (dx *= 0.5));
		if (fmid<=0.0) rtb = xmid;
		if(fabs(dx)<xacc || fmid==0) return rtb;
	}
	std::cout << "ERROR: too many iterations!" << std::endl;
}

double eljp (double x) {
	return e - 4*(pow(x, -12.0) - pow(x, -6.0));
}
