#include <iostream>
#include <cmath>
#include <iomanip>
#include "nrutil.h"
#define LOG(xname, x) std::cout << xname << x << std::endl
#define MAXIT 60
#define UNUSED (-1.11e30)

void nrerror(char error_text[])
/* Numerical Recipes standard error handler */
{
	fprintf(stderr,"Numerical Recipes run-time error...\n");
	fprintf(stderr,"%s\n",error_text);
	fprintf(stderr,"...now exiting to system...\n");
	exit(1);
}

// declaring constants

const double pi = 3.14159265359;

int main() {
	const double gamma = 21.7; // input constant for H2
	void search (int n, double e1, double enp);

	int nmax = gamma/pi * 0.841309 - 0.5;
	// for each n, the subroutine `search` looks up for
	// energy levels that are equal to (n+0.5)*pi*gamma
	double enp = -1.0;
	std::cout << "n\tx1\tx2\tEn" << std::endl;
	for (int n=0; n<=nmax; n++) {
		std::cout << n << "\t";
		double e1 = (n+0.5)*pi/gamma;
		search(n, e1, enp);
		std::cout << std::endl;
//		std::cout << "expected value = " << e1 << std::endl;
	}
	std::cout << std::endl;
	return 0;
} // end of main function

// the `search` subroutine starts from the bottom of the lj
// potential, calculates x1 and x2 for each step fixed by
// the variable `st` and compares it to the expected action
// `s1`. If action `s` is close enough to expected action `s1`
// the subroutine exits.

void search (int n, double e1, double enp) {
	double ljpi (double a, double b, int np, double e);
	double eljp (double x, double e);
	//double zriddr(double (*func)(double, double), double x1, double x2, double xacc, double e);
	double s, x1, x2;
	double estep = 1e-4;
	int maxstep = pow(estep, -1);
	double En = enp; // first guess of energy
	const double st = 1e-3; // if fabs(s1-s) < st ok!
	double nr0 (double x0, double e);
	double bisection (double a, double b, double e);

	for (int i=1; i<=maxstep; i++) {
		// we have turning points x1 and x2 only when the dimensionless
		// energy equals to the lj potential. This means that if we can
		// solve analitically for the roots of energy minus lj potential
		// we will have the exact roots (no need for nr!).
		En += estep;
		x1 = nr0(pow(2.0,1.0/6.0)-0.3, En);
		x2 = bisection(pow(2.0, 1.0/6.0), 20.0, En);
		//x2 = zriddr(eljp, pow(2.0,1.0/6.0), 50.0, 0.0001, En);
		double s = ljpi(x1, x2, 100, En);
		if (fabs(e1-s) < st || En >= 0) {
			enp = En;
			break;
		}
	}
	std::cout << x1 << "\t" << x2 << "\t" << std::setprecision(10) << enp;
}

double ljp (double x) {
	return 4.0*(pow(x, -12) - pow(x, -6));
}

double eljp (double x, double e) {
	return e - 4.0*(pow(x, -12) - pow(x, -6));
}

// definition of integrand function
double f1 (double x, double e) {
	return sqrt(fabs(e - 4*pow(x, -12) + 4*pow(x, -6)));
}

double ljpi (double a, double b, int np, double e) {
	double h, oddsum=0.0, evensum=0.0, sum=0.0;
	double ljp (double x);
	double f1 (double x, double e);
	h = (double) (b-a)/np;
	for (int i=1; i<=np/2; i++) {
		sum += f1(a+(2*i-2)*h, e) + 4.0*f1(a+(2*i-1)*h, e) + f1(a+(2*i)*h, e);
	}
	return sum*h/3.0;
}

double nr0 (double x0, double e) {
	double h = 1e-3;
	double x1, x2, xm1, xm2, fx0, fx1, fx2, fxm1, fxm2, f1x0, x;

	for(int i = 0; i <101; i++ ){
    	x1 = x0 + h;
    	x2 = x0 + 2*h;
    	xm1 = x0 - h;
    	xm2 = x0 - 2*h;

    	fx0 = 10*e - 40*(pow(x0, -12) - pow(x0, - 6));
    	fx1 = 10*e - 40*(pow(x1, -12) - pow(x1, - 6));
    	fx2 = 10*e - 40*(pow(x2, -12) - pow(x2, - 6));
    	fxm1 = 10*e - 40*(pow(xm1, -12)- pow(xm1, - 6));
    	fxm2 = 10*e - 40*(pow(xm2, -12) - pow(xm2, - 6));

    	f1x0 = (-fx2 + 8*fx1 - 8*fxm1 + fxm2)/(12*h);

    	x = x0 - fx0/f1x0;
    	x0 = x;
	}

	return x;
}

double bisection (double a, double b, double e) {
	double c = a;
	double eljp (double x, double e);
	double const epsilon = 0.001;
	while ((b-a) >= epsilon) {
		c = (a+b)/2;
		if (eljp(c, e) == 0.0)
			break;
		else if (eljp(c, e) * eljp(a, e) <0)
			b=c;
		else
			a=c;
	}
	return c;
}


double nr1 (double x0, double e){

	double h = 1e-5;
	double  x02, xz2, x1, x2, xm1, xm2, fx0, fx1, fx2, fxm1, fxm2, f1x0;

	x02 = x0+0.1;

for(int i = 0; i <101; i++ ){


	x1 = x02 + h;
	x2 = x02 + 2*h;
	xm1 = x02 - h;
	xm2 = x02 - 2*h;

	fx0 = 10*e - 40*(pow(x02, -12) - pow(x02, - 6));
	fx1 =  10*e - 40*(pow(x1, -12) - pow(x1, - 6));
	fx2 =  10*e - 40*(pow(x2, -12) - pow(x2, - 6));
	fxm1 =  10*e - 40*(pow(xm1, -12)- pow(xm1, - 6));
	fxm2 =  10*e - 40*(pow(xm2, -12) - pow(xm2, - 6));

	f1x0 = (-fx2 + 8*fx1 - 8*fxm1 + fxm2)/(12*h);

	xz2 = x02 - fx0/f1x0;
	x02 = xz2;

}

return x02;
}

double zriddr(double (*func)(double, double), double x1, double x2, double xacc, double e)
{
	int j;
	double ans, fh, fl, fm, fnew, s, xh, xl, xm, xnew;
	fl = (*func)(x1, e);
	fh = (*func)(x2, e);
	if ((fl > 0.0 && fh < 0.0) || (fl < 0.0 && fh > 0.0)) {
		x1 = x1;
		xh = x2;
		ans = UNUSED;
		for (j=1; j<=MAXIT; j++) {
			xm = 0.5*(xl+xh);
			fm = (*func)(xm, e);
			s = sqrt(fm*fm - fl*fh);
			if (s==0.0) return ans;
			xnew = xm + (xm-xl) * ((fl >= fh ? 1.0 : -1.0)*fm/s);
			if (fabs(xnew-ans) <= xacc) return ans;
			ans = xnew;
			fnew = (*func)(ans, e);
			if (fnew==0.0) return ans;
			if (SIGN(fm, fnew) != fm) {
				xl = xm;
				fl = fm;
				xh = ans;
				fh = fnew;
			} else if (SIGN(fl, fnew) != fl) {
				xh = ans;
				fh = fnew;
			} else if (SIGN(fh, fnew) != fh) {
				xl = ans;
				fl = fnew;
			} else nrerror("never get here.");
			if (fabs(xh-xl) <= xacc) return ans;
		}
		nrerror("zriddr exceed maximum iterations");
	}
	else {
		if (fl==0.0) return x1;
		if (fh==0.0) return x2;
		nrerror("root must be bracketed in zriddr.");
	}
	return 0.0;
}
