#include <iostream>
#include <cmath>
#include <iomanip>
#include <string>
#define MAXIT 60
#define ET 1e-10
#define XT 1e-10
#define NP 10000
#define PM pow(2.0, 1.0/6.0)
#define PI 3.14159265359
#define GAMMA 21.7
#define LOG(x) std::cout << x << std::endl

double x1, x2, en, e;
int n;

int main () {
	void search (int n, double se, double ep);
	int nmax = GAMMA/PI*0.841309-.5;
	double se, enp = -1.;
	std::string lddx1, lddx2;
	std::cout << "n\t| x1\t\t| x2\t\t| En" << std::endl;
	std::cout << "--------|---------------|---------------|----------" << std::endl;
	std::cout << std::setprecision(10);
	en = -0.1;
	for (n=0; n<=nmax; n++) {
		se = (n+.5)*PI/GAMMA;
		search(n, se, enp);
		enp = en;
		std::cout << n << "\t| " << x1 << "\t| " << x2 << "\t| " << en << std::endl;
	}
	std::cout << std::endl;
}

void search (int n, double se, double ep) {
	double f (double e);
	double em, e0=ep, e1=0., fm;
	for (int j=0; j<MAXIT; j++) {
		em = (e1+e0)/2;
		fm = f(em);
		fm >= 0 ? e1 = em : e0 = em;
		if (fabs(fm) < ET) {
			break;
		}
	}
	en = em;
}

double f (double e)
{
	double action (double e);
	return action(e) - (n+.5)*PI/GAMMA;
}

double action (double e) {
	double h, sum=0.0, dx, np=NP;
	double pot (double x);
	double nrs (double nrx0, double scale);
	
	x1 = PM;
	dx = .1;
	while (dx > ET) {
		x1 -= dx;
		if (pot(x1)>=e) { x1 += dx; dx /= 2; };
	}

	x2 = PM;
	dx = .1;
	while (dx > ET) {
		x2 += dx;
		if (pot(x2)>=e) { x2 -= dx; dx /= 2;}
	}

	h = (x2-x1)/np;
	for (int i=1; i<=np/2; i++) sum += sqrt(fabs(e-pot(x1+(2*i-2)*h))) + 4.0*sqrt(fabs(e-pot(x1+(2*i-1)*h))) + sqrt(fabs(e-pot(x1+(2*i)*h)));
	return sum*h/3.0;
}

double pot (double x) {
	return 4.0 * (pow(x, -12.0) - pow(x, -6.0));
}

double fscaled (double x, double scale) {
	double pot (double x);
	return scale*(e-pot(x));
}

double nrs (double nrx0, double scale)
{
	double nrx1, nrfx0, nrdfx0, nrh = 1e-3;
	double fscaled (double x, double scale);
	for (int k=0; k < MAXIT; k++) {
		nrfx0 = fscaled(nrx0, scale);
		if (fabs(nrfx0) < ET) break;
		nrdfx0 = (-3*fscaled(nrx0, scale)+4*fscaled(nrx0+nrh, scale)-fscaled(nrx0+2*nrh, scale))/(2*nrh);
		nrx1 = nrx0 - nrfx0/nrdfx0;
		nrx0 = nrx1;
	}
	return nrx0;
}
