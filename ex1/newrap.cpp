#include<iostream>
#include<cmath>

//newton-raphson method f(x) = E - 4(x^-12 -x^-6)

using namespace std;

int main() {


  double e;
  double h = pow(10, -5);
  double x0, x01, x02, xz1, xz2, x1, x2, xm1, xm2, fx0, fx1, fx2, fxm1, fxm2, f1x0, x;


  cout <<"\n E:   ";
  cin >> e;

  x0 = pow(2, 1/6);
  x01 = x0 - 0.1;
  x02= x0 + 0.3;


  for(int i = 0; i <101; i++ ){


    x1 = x01 + h;
    x2 = x01 + 2*h;
    xm1 = x01 - h;
    xm2 = x01 - 2*h;

    fx0 = 10*e - 40*(pow(x01, -12) - pow(x01, - 6));
    fx1 =  10*e - 40*(pow(x1, -12) - pow(x1, - 6));
    fx2 =  10*e - 40*(pow(x2, -12) - pow(x2, - 6));
    fxm1 =  10*e - 40*(pow(xm1, -12)- pow(xm1, - 6));
    fxm2 =  10*e - 40*(pow(xm2, -12) - pow(xm2, - 6));

    f1x0 = (-fx2 + 8*fx1 - 8*fxm1 + fxm2)/(12*h);

    xz1 = x01 - fx0/f1x0;
    x0 = xz1;

  }


  for(int i = 0; i <101; i++ ){


    x1 = x02 + h;
    x2 = x02 + 2*h;
    xm1 = x02 - h;
    xm2 = x02 - 2*h;

    fx0 = 10*e - 40*(pow(x02, -12) - pow(x02, - 6));
    fx1 =  10*e - 40*(pow(x1, -12) - pow(x1, - 6));
    fx2 =  10*e - 40*(pow(x2, -12) - pow(x2, - 6));
    fxm1 =  10*e - 40*(pow(xm1, -12)- pow(xm1, - 6));
    fxm2 =  10*e - 40*(pow(xm2, -12) - pow(xm2, - 6));

    f1x0 = (-fx2 + 8*fx1 - 8*fxm1 + fxm2)/(12*h);

    xz2 = x02 - fx0/f1x0;
    x02 = xz2;

  }


  cout << "x1 zero = " << xz1 <<"  x2 zero = "<< xz2 << endl;




  return 0;
}
