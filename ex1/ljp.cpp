#include <iostream>
#include <cmath>

int main() {
	double ljp (double x);
	double x=1.0-1e-2;
	while (x<=4) {
		std::cout << x << "," << ljp(x) << std::endl;
		x += 0.01;
	}
	return 0;
}

double ljp (double x)
{
	return 4*(pow(x, -12) - pow(x, -6));
}
