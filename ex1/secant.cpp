#include <iostream>
#include <iomanip>
#include <cmath>
#define JMAX 100

int main() {
	double myfunc (double x);
	double secant (double (*f)(double), double a, double b, double xacc);
	double zero = secant(myfunc, 0.0, 1.0, 0.000001);
	std::cout << std::setprecision(10) << zero << std::endl;
	return 0;
}

double secant (double (*f)(double), double a, double b, double xacc) {
	double xi, x0=a, x1=b, fi;
	if (f(a)*f(b) >= 0) {
		std::cout << "Interval not valid" << std::endl;
		exit(EXIT_FAILURE);
	}
	for (int i=0; i<JMAX; i++) {
		xi = -f(x0)*(x1-x0)/(f(x1)-f(x0)) + x0;
//		std::cout << xi << std::endl;
		fi = f(xi);
		std::cout << xi << std::endl;
		std::cout << x0 << std::endl;
		if (fabs(xi-x0)<xacc || fi==0) {
			break;
		}
		x0 = x1;
		x1 = xi;
	}
	return xi;
}

double myfunc (double x) {
	return 4.0*pow(x,3)-50.0*pow(x,2)+.5*x+30.0;
}
