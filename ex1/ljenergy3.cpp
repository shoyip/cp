#include<iostream>
#include<cmath>

const double pi = 3.14159265359;

int main() {

  const double gamma = 21.7;

  void search (int n, double e1, double eg);

std::cout << "n   x1   x2   En "<< std::endl;

  int nmax = gamma/pi * 0.841309 - 0.5;
  double eg = -1;

  for (int n = 0; n <= nmax; n++){
    double e1 = (n+0.5)*pi/gamma;
    search(n, e1, eg);


  }

  return 0;

}

void search (int n, double e1, double eg) {
  double vx (double x);
  double fx (double x, double e);
  double nr1 (double x0, double e);
  double nr2 (double x0, double e);
  double integral (double xz1, double xz2, double st, double e);
  double est = 1e-4;
  double max = pow(est, -1);
  double Ei = eg;
  double xmin = pow(2, 1/6);
  double ok = 1e-5;
  double xt1, xt2;

  for (int i = 1; i<= max; i++){

    Ei += est;

    xt1 = nr1(xmin, Ei );

    xt2 = nr2(xmin, Ei);

    double s = integral(xt1, xt2, 1000, Ei);

    if (fabs(e1 - s) <= ok || Ei >= 0){
      eg = s;
      break;
    }
  }
  std::cout << n << "\t" << xt1 << "\t" << xt2 << "\t" << Ei << std::endl;
}

double vx (double x){
  return 4.0*(pow(x, -12) - pow(x, -6));
}

double fx (double x, double e){
    return sqrt(fabs(e - vx(x)));
}

double integral (double xz1, double xz2, double st, double e){
  double vx (double x);
  double fx (double x,double e);
  double h;
  double in = 0;
  h = (xz2 - xz1)/st;
  for (int i=0; i<=st-1; i++){

    in = in + (fx(xz1+i*h, e) + fx( xz1+h*(i+1) , e))*h/2;
  }
return in;
}


double nr1 (double x0, double e) {
	double h = 1e-3;
	double x1, x2, xm1, xm2, fx0, fx1, fx2, fxm1, fxm2, f1x0, x;

  x0 = x0 - 0.1;

	for(int i = 0; i <101; i++ ){
    	x1 = x0 + h;
    	x2 = x0 + 2*h;
    	xm1 = x0 - h;
    	xm2 = x0 - 2*h;

    	fx0 = 10*e - 40*(pow(x0, -12) - pow(x0, - 6));
    	fx1 = 10*e - 40*(pow(x1, -12) - pow(x1, - 6));
    	fx2 = 10*e - 40*(pow(x2, -12) - pow(x2, - 6));
    	fxm1 = 10*e - 40*(pow(xm1, -12)- pow(xm1, - 6));
    	fxm2 = 10*e - 40*(pow(xm2, -12) - pow(xm2, - 6));

    	f1x0 = (-fx2 + 8*fx1 - 8*fxm1 + fxm2)/(12*h);

    	x = x0 - fx0/f1x0;
    	x0 = x;
	}

	return x0;
}

double nr2 (double x0, double e){

	double h = 1e-5;
	double  x02, xz2, x1, x2, xm1, xm2, fx0, fx1, fx2, fxm1, fxm2, f1x0;

	x02 = x0+0.2;

for(int i = 0; i <101; i++ ){


	x1 = x02 + h;
	x2 = x02 + 2*h;
	xm1 = x02 - h;
	xm2 = x02 - 2*h;

	fx0 = 10*e - 40*(pow(x02, -12) - pow(x02, - 6));
	fx1 =  10*e - 40*(pow(x1, -12) - pow(x1, - 6));
	fx2 =  10*e - 40*(pow(x2, -12) - pow(x2, - 6));
	fxm1 =  10*e - 40*(pow(xm1, -12)- pow(xm1, - 6));
	fxm2 =  10*e - 40*(pow(xm2, -12) - pow(xm2, - 6));

	f1x0 = (-fx2 + 8*fx1 - 8*fxm1 + fxm2)/(12*h);

	xz2 = x02 - fx0/f1x0;
	x02 = xz2;

}

return x02;
}
