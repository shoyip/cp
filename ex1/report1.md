---
documentclass: article
classoption: a4paper
lang: it-IT
nocite: '@*'
numbersections: true
title: Quantizzazione Semiclassica di Livelli Vibrazionali per Molecole Diatomiche
author: Matteo Bettanin \and Shoichi Yip
affiliation: Università di Trento
header-includes: |
	\usepackage{fvextra}
	\DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
    \usepackage{mathptmx}
    \usepackage[version=4]{mhchem}
    \usepackage{siunitx}
abstract: |
	In questa esercitazione troviamo numericamente le energie degli stati
	legati di alcune molecole diatomiche (\ce{H2}, \ce{OH}, \ce{O2}) utilizzando
	l'approssimazione semiclassica WKB.
...

# Introduzione

Le molecole diatomiche che vogliamo studiare (\ce{H2}, \ce{OH},
\ce{O2}) consistono di due nuclei legati dalle proprie nubi elettroniche. A
grandi distanze il potenziale è attrattivo (*interazioni di Van der Waals*),
mentre a brevi distanze è repulsivo (*interazione Coulombiana*): una buona
approssimazione di questo comportamento è rappresentato dal *potenziale di
Lennard-Jones*, definito come
$$ 
V(r) = 4\epsilon \left[ \left( \frac{\sigma}{r} \right)^{12} -
\left( \frac{\sigma}{r} \right)^6 \right]. 
$$ {#eq:ljp}
Questo potenziale, di cui noi consideriamo soltanto i valori per $r$ positivi in
quanto le distanze sono positive, diverge per $r\to0$ e possiede un minimo
$-\epsilon$ in $r_{\text{min}} = 2^{1/6}\sigma$. Per $r\to\infty$ invece
converge a zero.

Per trovare le energie degli stati legati di una molecola diatomica dobbiamo
trovare gli autovalori della seguente equazione di Schrödinger unidimensionale
$$ \left[ - \frac{\hbar^2}{2m} \frac{d^2}{dr^2} + V(r) \right]
\psi_n = E_n \psi_n$$ {#eq:schrodinger}
dove $m$ è la massa ridotta dei due nuclei. Invece di risolvere l'equazione
differenziale, possiamo prendere in considerazione una ulteriore
approssimazione: dato che il moto dei nuclei è quasi classico, viste le loro
grandi masse, possiamo considerare in prima istanza il moto classico dei nuclei
nel potenziale in Eq. @eq:ljp e successivamente applicare le regole di
quantizzazione. Questa è l'approssimazione semiclassica WKB applicata
all'Eq. @eq:schrodinger, e prevede la regola
$$
\int_{x_1}^{x^2} dx \sqrt{2m [E-V(x)]} = \left( n + \frac{1}{2} \right) \pi \hbar\;.
$$ {#eq:quantization}

# Quesiti

## WKB per l'Oscillatore Armonico

Verifichiamo prima di tutto che se cerchiamo i livelli energetici dell'oscillatore
utilizzando le condizioni di quantizzazione date dall'approssimazione WKB
troviamo le soluzioni esatte fornite dalla teoria.
Richiediamo dunque che
$$
    E=V(x)=\frac{1}{2}m\omega^2x^2
$$
e, risolvendo l'equazione di secondo grado otteniamo che i punti di inversione sono
$$
    x_{1,2} = \mp\sqrt{\frac{2E}{m\omega^2}}\;.
$$
Applichiamo dunque questi punti di inversione alla formula
$$
    \int_{x_1}^{x_2} dx \sqrt{2m[E-\frac{1}{2}m\omega^2x^2]} = \left( n + \frac{1}{2} \right) \pi \hbar \;.
$$
Risolviamo l'integrale al primo termine: per semplificare i calcoli possiamo eseguire la sostituzione $u=\omega x\sqrt{m/2E}$. In questo modo otteniamo l'integrale $\int_{-1}^{+1}\sqrt{1-u^2}$, del cui integrale definito conosciamo la soluzione tabulata $\int \sqrt{1-u^2} = u\sqrt{1-u^2}/2+\arcsin(u)/2+\text{c}$.
Otteniamo quindi l'equazione
$$
    E\frac{\pi}{\omega} = \left( n + \frac{1}{2} \right) \pi \hbar
$$
che a meno di fattorizzazione dà come risultato i livelli energetici dell'oscillatore armonico, ovvero
$$
    E = \left( n + \frac{1}{2} \right) \hbar \omega\;.
$$

## WKB per Lennard-Jones

La condizione sulle energie in Eq. @eq:quantization
si può riscrivere nei termini adimensionali $x=r/\sigma$ e $\tilde E = E/\varepsilon$ del potenziale di Lennard-Jones come
$$
    \sqrt{2m\varepsilon\sigma^{2}} \int_{x_1}^{x_2} dx \sqrt{\tilde E-\frac{4}{x^{12}} + \frac{4}{x^6}} - \pi \left( n + \frac{1}{2} \right) \hbar = 0\;.
$$

Se fattorizziamo tutto per $1/\sqrt{2m\varepsilon\sigma^2}$ possiamo riscrivere la condizione come
$$
    \int_{x_1}^{x_2} dx \sqrt{\tilde E-\frac{4}{x^{12}} + \frac{4}{x^6}} = \frac{\pi}{\gamma} \left( n + \frac{1}{2} \right)
$$
dove $\gamma = \sqrt{2m\varepsilon\sigma^2/\hbar^2 }$ è un parametro che
conosciamo per le molecole diatomiche \ce{H2}, \ce{OH} e \ce{O2}.
Per le tre molecole $\gamma$ vale rispettivamente $21.7$, $24.8$ e $150$.

## Livelli Energetici di LJ in approssimazione WKB

Con il nostro programma vogliamo trovare, per diversi valori di
$n=0,1,2,3,\dots$, i valori di energia $E_n$ che soddisfano le condizioni
dell'approssimazione WKB. Usando le sostituzioni precedenti e normalizzando per
$\tilde E$ abbiamo
$$
v(x) = 4 (x^{-12} - x^{-6}).
$$

Il programma procede nel seguente modo:

1. Prima di tutto troviamo il numero $n$ del massimo livello energetico ammesso
   come il *floor* di $$ n_{max} = \frac{\gamma}{\pi} \int_1^{+\infty}dx
\,\sqrt{-v(x)} -\frac{1}{2}. $$
2. A questo punto eseguiamo un ciclo sugli $n$ livelli energetici fino a
   $n_{max}$, e per ogni livello avviamo la routine `search`.
3. La routine `search` esegue un ciclo sulle energie in cui cerchiamo lo zero
   della funzione ausiliaria $$f(\tilde E) = S(\tilde E) - \frac{\pi}{\gamma} \left( n +
\frac{1}{2} \right), $$ dove $S$ è l'integrale definito $$ \int_{x_1}^{x_2} dx
\sqrt{\tilde E - v(x)}. $$  In ogni ciclo di ricerca dell'energia partiamo
dall'ultimo livello calcolato $E_{n-1}$, con il punto di partenza del primo
ciclo -1. Similmente troviamo gli zeri $x_1, x_2$ della funzione $$ \tilde E -
V(x).$$
4. Dati gli estremi di integrazione  $x_1, x_2$ calcoliamo l'integrale d'azione
   $S$ (subroutine `action`) con il Metodo di Simpson 1/3 a \num{1e4} partizioni.

Abbiamo scelto di implementare il metodo della bisezione per la ricerca degli
zeri perché nonostante il metodo di Newton-Raphson o delle secanti permettano di
avere una maggiore velocità computazionale non garantivano la convergenza per le
funzioni da noi studiate. Ad esempio, se scegliamo di impiegare il metodo di
Newton-Raphson nella ricerca degli zeri dell'integrando $\tilde E - v(x)$,
verifichiamo che tale ricerca diverge molto facilmente sia per $x_1$ che per
$x_2$. Infatti abbiamo comportamenti alla derivata poco regolari attorno agli
zeri e nel nostro problema abbiamo un ulteriore *constraint*, infatti
richiediamo che $x>0$. Nella ricerca degli zeri abbiamo impostato in tutti i
casi una tolleranza di \num{1e-10}.

# Esecuzione ed esiti del programma

L'esecuzione del programma, che abbiamo scritto in linguaggio C++ e compilato con
lo *GNU C Compiler*, richiede \SI{0.19}{\sec} per $\gamma=21.7$ e $24.8$, mentre
nel caso di $\gamma = 150$ richiede \SI{1.19}{\sec}.

$n$       | $x_1$            | $x_2$            | $E_n$
--------|---------------|---------------|----------
0       | 1.051817526   | 1.250522139   | -0.7724480201
1       | 1.021549308   | 1.423713292   | -0.4226382013
2       | 1.008860456   | 1.63917521    | -0.1955785095
3       | 1.002901366   | 1.967617985   | -0.06774323824
4       | 1.000525253   | 2.612440326   | -0.01254334565
5       | 1.000007037   | 5.358946169   | -0.0001688749709

Table: Estremi di integrazione e valori di energia adimensionale degli stati legati per $\gamma=21.7$

$n$ | $x_1$           | $x_2$              | $E_n$
--|--------------|-----------------|------------------
0 | 1.055245468  | 1.239490678     | -0.7988760998
1 | 1.025222702  | 1.389700873     | -0.4782144814
2 | 1.011869554  | 1.563904894     | -0.2547128108
3 | 1.004937701  | 1.802876315     | -0.1130913936
4 | 1.001536434  | 2.185803747     | -0.03634063215
5 | 1.000232168  | 2.992723865     | -0.005559749263
6 | 1.000000442  | 8.501072563     | -1.059781908e-05

Table: Estremi di integrazione e valori di energia adimensionale degli stati legati per $\gamma=24.8$

$n$       | $x_1$            | $x_2$            | $E_n$
--------|---------------|---------------|----------
0       | 1.09073637    | 1.162037152   | -0.9647641191
1       | 1.071511122   | 1.197405047   | -0.8966715384
2       | 1.059960078   | 1.225723221   | -0.8317033639
3       | 1.051489588   | 1.25162791    | -0.7698014017
4       | 1.044786649   | 1.276478883   | -0.7109065211
5       | 1.039259099   | 1.300919783   | -0.6549586318
6       | 1.034581481   | 1.325332893   | -0.6018966562
7       | 1.030552988   | 1.349981923   | -0.5516584974
8       | 1.02703981    | 1.375070523   | -0.5041810169
9       | 1.023947711   | 1.400770716   | -0.4594
10      | 1.02120749    | 1.427238636   | -0.4172501293
11      | 1.01876663    | 1.454624306   | -0.3776649542
12      | 1.016584196   | 1.483078441   | -0.3405768608
13      | 1.014627568   | 1.51275778    | -0.305917042
14      | 1.012870253   | 1.543829757   | -0.2736154684
15      | 1.01129038    | 1.576477055   | -0.2436008561
16      | 1.00986963    | 1.610902409   | -0.2158006433
17      | 1.008592457   | 1.647334035   | -0.1901409538
18      | 1.007445511   | 1.686032001   | -0.1665465785
19      | 1.006417197   | 1.727296005   | -0.1449409438
20      | 1.005497342   | 1.771475058   | -0.1252460909
21      | 1.004676922   | 1.818979834   | -0.1073826524
22      | 1.003947864   | 1.870298648   | -0.09126983609
23      | 1.003302868   | 1.926018551   | -0.07682540758
24      | 1.002735274   | 1.986853609   | -0.06396567951
25      | 1.002238948   | 2.053683536   | -0.05260550672
26      | 1.001808188   | 2.127607573   | -0.04265828244
27      | 1.001437648   | 2.210021221   | -0.03403594739
28      | 1.001122268   | 2.302728438   | -0.02664899995
29      | 1.00085722    | 2.408110383   | -0.02040651724
30      | 1.00063786    | 2.529387845   | -0.01521618725
31      | 1.000459684   | 2.671045872   | -0.01098434988
32      | 1.000318296   | 2.839554342   | -0.007616046932
33      | 1.000209378   | 3.044663406   | -0.005015090023
34      | 1.000128663   | 3.30190939    | -0.003084138265
35      | 1.000071916   | 3.637943554   | -0.001724792539
36      | 1.000034916   | 4.103422442   | -0.0008377060157
37      | 1.000013448   | 4.810632479   | -0.0003227095923
38      | 1.00000329    | 6.082893665   | -7.895689669e-05
39      | 1.000000212   | 9.607886768   | -5.085014346e-06

Table: Estremi di integrazione e valori di energia adimensionale degli stati legati per $\gamma=150$

Possiamo plottare i valori di energia degli stati legati e confrontarli al
potenziale come si vede nelle seguenti figure.

![Potenziale di Lennard-Jones e stati legati dell'\ce{H2}](fig2.pdf)

![Potenziale di Lennard-Jones e stati legati dell'\ce{HO}](fig3.pdf)

![Potenziale di Lennard-Jones e stati legati dell'\ce{O2}](fig1.pdf)

# Listato del programma

```cpp
#include <iostream>
#include <cmath>
#include <iomanip>
#include <string>
#define MAXIT 60
#define ET 1e-10
#define XT 1e-10
#define NP 10000
#define PM pow(2.0, 1.0/6.0)
#define PI 3.14159265359
#define GAMMA 21.7
#define LOG(x) std::cout << x << std::endl

double x1, x2, en, e;
int n;

int main () {
	void search (int n, double se, double ep);
	int nmax = GAMMA/PI*0.841309-.5;
	double se, enp = -1.;
	std::string lddx1, lddx2;
	std::cout << "n\t| x1\t\t| x2\t\t| En" << std::endl;
	std::cout << "--------|---------------|---------------|----------" << std::endl;
	std::cout << std::setprecision(10);
	en = -0.1;
	for (n=0; n<=nmax; n++) {
		se = (n+.5)*PI/GAMMA;
		search(n, se, enp);
		enp = en;
		std::cout << n << "\t| " << x1 << "\t| " << x2 << "\t| " << en << std::endl;
	}
	std::cout << std::endl;
}

void search (int n, double se, double ep) {
	double f (double e);
	double em, e0=ep, e1=0., fm;
	for (int j=0; j<MAXIT; j++) {
		em = (e1+e0)/2;
		fm = f(em);
		fm >= 0 ? e1 = em : e0 = em;
		if (fabs(fm) < ET) {
			break;
		}
	}
	en = em;
}

double f (double e)
{
	double action (double e);
	return action(e) - (n+.5)*PI/GAMMA;
}

double action (double e) {
	double h, sum=0.0, dx, np=NP;
	double pot (double x);

	x1 = PM;
	dx = .1;
	while (dx > ET) {
		x1 -= dx;
		if (pot(x1)>=e) { x1 += dx; dx /= 2; };
	}

	x2 = PM;
	dx = .1;
	while (dx > ET) {
		x2 += dx;
		if (pot(x2)>=e) { x2 -= dx; dx /= 2;}
	}

	h = (x2-x1)/np;
	for (int i=1; i<=np/2; i++) sum += sqrt(fabs(e-pot(x1+(2*i-2)*h))) + 4.0*sqrt(fabs(e-pot(x1+(2*i-1)*h))) + sqrt(fabs(e-pot(x1+(2*i)*h)));
	return sum*h/3.0;
}

double pot (double x) {
	return 4.0 * (pow(x, -12.0) - pow(x, -6.0));
}
```

# Riferimenti
