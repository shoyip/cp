// Implementation of Newton Raphson method

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <string>

int main() {
	const double eps = 1.0e-9;
	const int NMAX = 1000;
	double x0, x1, err, f, Df;
	err = 1.0;
	std::string buf;
	std::cout << "Enter x0:" << std::endl;
	std::cin >> x0; getline(std::cin, buf);
	std::cout << "i	" << "x1	" << "err" << std::endl;
	std::cout.precision(4);

	for (int i=1; i<=NMAX; i++) {
		f = 3*x0*x0 + 4*x0 - 5;
		Df = 6*x0 + 4;
		x1 = x0 - f/Df;
		err = sqrt((x1-x0)*(x1-x0));
		std::cout << i << "	" << x1 << "	" << err << std::endl;
		if(err<eps) break;
		x0 = x1;
	}
}
