#include <iostream>
#include <cmath>

int main() {
	double ljp (double x);
	double x=0.001;
	while (x<=2) {
		std::cout << ljp(x) << std::endl;
		x+=0.001;
	}
	return 0;
}

double ljp (double x) {
	return 4*(pow(x,-12) - pow(x,-6));
}
