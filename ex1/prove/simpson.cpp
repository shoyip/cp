// Implementation of Simspon's rule

#include <iostream>
#include <cmath>

int main() {
	double integrate (double a, double b, int N);
	double myInt = integrate(0.0, 11.2, 10);
	std::cout << "The integral value is " << myInt << std::endl;
	return 0;
}

double integrate (double a, double b, int N) {
	double h, x[N+1], sum=0;
	double f (double x);
	int i;
	x[0]=a;
	h = (b-a)/N;
	for (i=1; i<=N; i++) x[i] = a+i*h;
	for (i=1; i<=N/2; i++) sum += f(x[2*i-2]) + 4*f(x[2*i-1]) + f(x[2*i]);

	return sum*h/3.0;
}

double f (double x) {
	return 3*x*x+4*x-5;
}
