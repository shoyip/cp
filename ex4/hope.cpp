#include<iostream>
#include<cmath>
#include<fstream>
#include<random>

const double kb = 1.380649e-23;
double eps = 1; //3.20435313e-21; //J
double sigma =1;// 3.94e-10; //m
double delt = 1e-14; //s
double T = 300; //K
const int n = 5;
int maxp = n*n*n;
double m = 1; //2.2e-25; //Kg
double steps = 1e5;
double rho = 1e27;
int c = 0;
int i = 0;
double a[3];
double V = 0;
double K = 0; //energia cinetica
double E = 0;
double lj(double r);
double ljf(double r);
std::ofstream ofile;

int main(){
	struct part{
		double x,y,z,vx,vy,vz,fx,fy,fz,ax,ay,az;
	};
	struct part p[maxp];
	/*struct mirr{
		double x,y,z;
	};*/
	//struct mirr m[3250];
	
	double dx = 1e-9;
	double L = n*1e-9;
	double x0[n];
	double y0[n];
	double z0[n];
	double r2[maxp][maxp];
	double r[maxp][maxp];
	double dvar = sqrt(kb*T/m);
	double ts;
	double v0[125];
	double v02[125];
	double vt[125];
	double fcorr[500];
	int u = 0;
	
	for (int i = 0; i<n; i++){
		x0[i] = i*dx+0.5*dx; 
		y0[i] = i*dx+0.5*dx;
		z0[i] = i*dx+0.5*dx;
	}

	//inizializzo le posizioni su un reticolo.
	for(int i=0;i<n; i++){
		for(int j=0;j<n; j++){
			for(int k=0;k<n; k++){
			p[c].x = x0[i];
			p[c].y = y0[j];
			p[c].z = z0[k];
			//std::cout<<p[c].x<<"	"<<p[c].y<<"	"<<p[c].z<<std::endl;
			c += 1;
			}
		}
	}	
	
	
	//inizializzo le velocità iniziali secondo boltzmann
	std::random_device rd;
	std::mt19937 gen(rd());
	
	for(i = 0; i<maxp ; i++){
		std::normal_distribution<> d(0, 1);
		p[i].vx = d(gen);
		p[i].vy = d(gen);
		p[i].vz = d(gen);
		//std::cout<<p[i].vx<<std::endl;
	}
	
	//calcolo le distanze tra le particelle
	
	for (int i=0; i<maxp; i++){
		int j = 0;
		while(j<i){
			r2[i][j] = pow(p[i].x-p[j].x, 2.0)+pow(p[i].y-p[j].y, 2.0)+pow(p[i].z-p[j].z, 2.0); 
			r[i][j] = sqrt(r2[i][j]);
			//std::cout<<r[i][j]<<"	";
			r[j][i] = -r[i][j];
			j += 1;
			
			}
			//std::cout<<std::endl;
	}
	
	//interazioni al tempo 0:
		double phi, theta;
		for (int i=0; i<maxp; i++){
			
			for(int j = 0; j<maxp; j++){
				if(j!=i){
				phi = acos((p[i].z-p[j].z)/r[i][j]);
				theta = atan2(p[i].y - p[j].y, p[i].x - p[j].x);
				p[i].fx += -ljf(r[i][j])*sin(phi)*cos(theta);
				p[i].fy += -ljf(r[i][j])*sin(phi)*sin(theta);
				p[i].fz += -ljf(r[i][j])*cos(phi);
				j += 1;
				
				}
			}
			//std::cout<<phi<<"	"<<theta<<std::endl;
		}
	
		
	//ciclo sui tempi
	ofile.open("etoth.dat");
	for(int t = 0; t<steps; t++){//sarebbe sul tempo
		ts = delt*t;
		
		// calcolo V e T
		for(int i=0; i<maxp; i++){
			for(int j=0; j<i; j++){
				V += lj(r[i][j]);
				//std::cout<<V<<std::endl;
			} 
			K += 0.5*m*(pow(p[i].vx, 2)+pow(p[i].vy, 2)+pow(p[i].vz, 2));
		}
		E = V+K;
		
		ofile<<ts<<","<<V<<","<<K<<","<<E<<std::endl;
		
		
		
		//velocity verlet
		for(i= 0; i<maxp; i++){
			p[i].ax = p[i].fx/m;
			p[i].ay = p[i].fy/m;
			p[i].az = p[i].fz/m;
			
			p[i].x = p[i].x + p[i].vx*delt + 0.5*p[i].ax*pow(delt, 2.0);
			p[i].y = p[i].y + p[i].vy*delt + 0.5*p[i].ay*pow(delt, 2.0);
			p[i].z = p[i].z + p[i].vz*delt + 0.5*p[i].az*pow(delt, 2.0);
			//std::cout<<p[i].x<<std::endl;
		}	
			//condizioni periodiche
		
			for(int i=0;i<maxp; i++){
				if(p[i].x  > L){
					p[i].x -= L;
				}
				if(p[i].y  > L){
					p[i].y -= L;
				}
				if(p[i].z  > L){
					p[i].z -= L;
				}
				if(p[i].x  < 0){
					p[i].x += L;
				}
				if(p[i].y  < 0){
					p[i].y += L;
				}
				if(p[i].z  < 0){
					p[i].z += L;
				}
		    }	
			//ricalcolo le distanze tra le particelle
	
			for (int i=0; i<maxp; i++){
				int j = 0;
				while(j<i){
					r2[i][j] = pow(p[i].x-p[j].x, 2.0)+pow(p[i].y-p[j].y, 2.0)+pow(p[i].z-p[j].z, 2.0); 
					r[i][j] = sqrt(r2[i][j]);
					r[j][i] = -r[i][j];
					//std::cout<<r[i][j]<<"	";
					j += 1;
			
				}
				//std::cout<<std::endl;
			}
			
			//ricalcolare le interazioni con le nuove posizioni
			for (int i=0; i<maxp; i++){
				p[i].fx = 0;
				p[i].fy = 0;
				p[i].fz = 0;
				
				for(int j = 0; j<maxp; j++){
					if(j!=i){
						phi = acos((p[i].z-p[j].z)/r[i][j]);
						theta = atan2(p[i].y - p[j].y, p[i].x - p[j].x);;
						p[i].fx += -ljf(r[i][j])*sin(phi)*cos(theta);
						p[i].fy += -ljf(r[i][j])*sin(phi)*sin(theta);
						p[i].fz += -ljf(r[i][j])*cos(phi);
						j += 1;
					}
				}
			}
			
			for(int i =0; i<maxp; i++){
			//a(t + dt) e v(t+dt)
			a[0] = p[i].fx/m;
			a[1] = p[i].fy/m;
			a[2] = p[i].fz/m;
			p[i].vx = p[i].vx + 0.5*(a[0] + p[i].ax)*delt;
			p[i].vy = p[i].vy + 0.5*(a[1] + p[i].ay)*delt;
			p[i].vx = p[i].vz + 0.5*(a[2] + p[i].az)*delt;
			//std::cout<<p[i].vx<<std::endl;
			}
		
		V = 0;
		E = 0;
		K = 0;
		/*
		if(steps-t==500){
			for(int k =0; k<maxp; k++){
				v0[k] = sqrt(pow(p[i].vx,2) + pow(p[i].vy, 2) + pow(p[i].vz,2));
				v02[k] = pow(p[i].vx,2) + pow(p[i].vy, 2) + pow(p[i].vz,2);
			}
		}
		if(steps-t<501){
			for(int k=0; k<maxp; k++){
				vt[k]=sqrt(pow(p[i].vx,2) + pow(p[i].vy, 2) + pow(p[i].vz,2));
				fcorr[u] += vt[k]*v0[k]/v02[k]; 
			}
			u+=1;
		}
        */
	} 	
	std::cout<<std::endl;
	ofile.close();
	return 0;
}




//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*eps*(pow(sigma/r,12)-pow(sigma/r, 6));
	}
	
}
//funzione per la forza a partire dal potenziale lj.
double ljf(double r){
	return 24*eps*pow(sigma, 6)*(2*pow(sigma,6)*pow(r,-13)-pow(r,-7 ));
	
}

