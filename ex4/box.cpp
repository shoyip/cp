#include <iostream>
#include <cmath>
#include <fstream>
#include <random>

const double kb = 1.38064852e-23;
double eps = 3.20435e-21; // j
double sigma = 1; // 10e-10 m
double delt = 1*pow(10, -12.0); // s
double T = 300; // K
const int n = 5;
double m = 2.2e-25; // Kg
double steps = 1e6;
double rho = 1e27;
int c=0;
int i = 0;
double a[3];
double a1[3];
double V = 0;
double K = 0; // energia cinetica
double E = 0;
double lj(double r);
double ljf(double r);

struct part {
	double x,y,z,vx,vy,vz,fx,fy,fz;
};
void initialize(part p, double dx);

int main(){
	struct part p[125];
	double L = pow(rho, -1.0/3.0);
	double dx = L/n;
	double x0[n];
	double y0[n];
	double z0[n];
	double r2[125][125];
	double r[125][125];
	double dvar = kb*T/m;
	int c=0;
	initialize(p, dx);
	
	// ciclo sui tempi
	for(int t = 0; t<2; t++){ // sarebbe sul tempo
		
	} 	
	return 0;
}

void initialize (part p, double dx ) {
	// inizializzo le posizioni su un reticolo.
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			for(int k=0; k<n; k++){
			p[c].x = i*dx;
			p[c].y = j*dx;
			p[c].z = k*dx;
			std::cout<<p[c].x<<"	"<<p[c].y<<"	"<<p[c].z<<std::endl;
			c += 1;
			}
		}
	}	
	
	// inizializzo le velocità iniziali secondo boltzmann
	std::random_device rd;
	std::mt19937 gen(rd());
    
	for(i = 0; i<125 ; i++){
		std::normal_distribution<> d(0, dvar);
		p[i].vx = d(gen);
		p[i].vy = d(gen);
		p[i].vz = d(gen);
		//std::cout<<p[i].vx<<std::endl;
	}
}

void update () {
		// calcolo le distanze tra le particelle
	
		for (int i=0; i<125; i++){
			int j = 0;
			while(j<i){
				r2[i][j] = pow(p[i].x-p[j].x, 2.0)+pow(p[i].y-p[j].y, 2.0)+pow(p[i].z-p[j].z, 2.0); 
				r[i][j] = sqrt(r2[i][j]);
				//std::cout<<r[i][j]<<"	";
				j += 1;
			
			}
			//std::cout<<std::endl;
		}
		
	    
		// calcolo V e T
		for(i=0; i<125; i++){
			for(int j=0; j<i; j++){
				V += lj(r[i][j]);
			} 
			K += 0.5*m*(pow(p[i].vx, 2)+pow(p[i].vy, 2)+pow(p[i].vz, 2));
		}
		E = V+K;
		std::cout<<V<<std::endl;
		
		// interazioni al tempo 0:
		double phi, theta;
		for (int i=0; i<125; i++){
			
			for(int j = 0; j<125; j++){
				if(j!=i){
				phi = acos((p[i].z-p[j].z)/r[i][j]);
				theta = acos((p[i].z-p[j].z)/(r[i][j]*sin(theta)));
				p[i].fx += ljf(r[i][j])*sin(theta)*cos(phi);
				p[i].fy += ljf(r[i][j])*sin(theta)*sin(phi);
				p[i].fz += ljf(r[i][j])*cos(phi);
				j += 1;
				}
			}
		}
		
		//velocity verlet
		for(i= 0; i<125; i++){
			a[0] = p[i].fx/m;
			a[1] = p[i].fy/m;
			a[2] = p[i].fz/m;
			p[i].x = p[i].x + p[i].vx*delt + 0.5*a[0]*pow(delt, 2.0);
			p[i].y = p[i].y + p[i].vy*delt + 0.5*a[1]*pow(delt, 2.0);
			p[i].z = p[i].z	+ p[i].vz*delt + 0.5*a[2]*pow(delt, 2.0);
			
			// condizioni periodiche
		
			for(int i=0; i<125; i++){
				if(p[i].x  > L){
					p[i].x -= L;
				}
				if(p[i].y  > L){
					p[i].y -= L;
				}
				if(p[i].z  > L){
					p[i].z -= L;
				}
				if(p[i].x  < 0){
					p[i].x += L;
				}
				if(p[i].y  < 0){
					p[i].y += L;
				}
				if(p[i].z  < 0){
					p[i].z += L;
				}
		    }	
            
			//ricalcolo le distanze tra le particelle
	
			for (int i=0; i<125; i++){
				int j = 0;
				while(j<i){
					r2[i][j] = pow(p[i].x-p[j].x, 2)+pow(p[i].y-p[j].y, 2)+pow(p[i].z-p[j].z, 2); 
					r[i][j] = sqrt(r2[i][j]);
					j += 1;
				}
			}
			
			//ricalcolare le interazioni con le nuove posizioni
			for (int i=0; i<125; i++){
				int j = 0;
				for(j = 0; j<125; j++){
					phi = acos((p[i].z-p[j].z)/r[i][j]);
					theta = acos((p[i].z-p[j].z)/(r[i][j]*sin(theta)));
					p[i].fx += ljf(r[i][j])*sin(theta)*cos(phi);
					p[i].fy += ljf(r[i][j])*sin(theta)*sin(phi);
					p[i].fz += ljf(r[i][j])*cos(phi);
					j += 1;
				}
			}
			//a(t + dt) e v(t+dt)
			a1[0] = p[i].fx/m;
			a1[1] = p[i].fy/m;
			a1[2] = p[i].fz/m;
			p[i].vx = p[i].vx + 0.5*(a[0] + a1[0])*delt;
			p[i].vy = p[i].vy + 0.5*(a[1] + a1[1])*delt;
			p[i].vx = p[i].vz + 0.5*(a[2] + a1[2])*delt;
		}
		V = 0;
		E = 0;
		K = 0;
}

// Funzione del potenziale Lennard-Jones.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*eps*(pow(sigma/r,12)-pow(sigma/r, 6));
	}
	
}
// Funzione Forza calcolata come Derivata del potenziale Lennard-Jones
double ljf(double r){
	return 24*eps*pow(sigma, 6)*(2*pow(sigma,6)*pow(r,-7)-pow(r,-4 ));
	
}
