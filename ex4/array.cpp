#include <iostream>
#include <cmath>

void func ( int input[] );

int main() {
    int array[10] = {5, 30, 52, 31, 245, 73, 95, 48, 385, 2};
    for (int i=0; i<10; i++) {std::cout << array[i] << " "; }
    std::cout << std::endl;
    func(array);
    for (int i=0; i<10; i++) {std::cout << array[i] << " "; }
    std::cout << std::endl;
    return 0;
}

void func ( int input[] ) {
    for (int i=0; i<10; i++) {
        input[i] *= 2;
    }
}
