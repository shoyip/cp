#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
#include <random>
#include <stdlib.h>
#define PREC 1.e-4

int main (int argc, char *argv[]);
double boxmuller (double mu, double sigma);
void initialize (int, int, double dx, std::vector<std::vector<double>>* x, std::vector<std::vector<double>>* v, std::vector<std::vector<double>>* a);
void update (std::vector<std::vector<double>>* x, std::vector<std::vector<double>>* v, std::vector<std::vector<double>>* a, std::vector<std::vector<double>>* f, std::vector<std::vector<double>>* r, double* kin, double* pot);
void refold (std::vector<std::vector<double>>* x);
double lj (double r);
double ljf (double r);
double derivative (double x, double h, double func(double x));

double kB = 8.6173e-5; // eV / K
double e = 1.6021e-19;
double NA = 6.02214076e23;
double PI = 3.141592653589793;
double dx;
double v_vel;
double L;
int N;
int dim=3;

// Parametri di Controllo
int n;          // numero particelle per lato
int Nsteps;     // numero di timesteps
double deltat;  // lunghezza adimensionale di timesteps
double rho_req; // densità richiesta
double T;       // temperatura richiesta

// Parametri statistici
double kin, pot;
double kin_sum = 0.0;   // somma di tutti i valori di energia cinetica
double pot_sum = 0.0;   // somma di tutti i valori di energia potenziale

// Proprietà dell'elemento
double eps = 0.02; // eV
double sigma = 3.94; // Angstrom
double m = 131.293 * 1.6654e-27; // Xenon mass

std::vector<double> rij_arr;

int main(int argc, char *argv[]) {
    int s;
    n = 5;
    Nsteps = 1;
    //deltat = 1.e-4 * sqrt(eps*e / m * pow(sigma, -2.));
    deltat = 1e-12;
    std::cout << " delta t = " << deltat << std::endl;
    T = 300.*kB/eps;
    dx = 10./sigma;
    v_vel = 300.*kB;
    N = n*n*n;
    L = 50./sigma;
    std::cout << "L=" << L << std::endl;
    std::vector<std::vector<double>> r;
    r.reserve(10000);

    std::vector<std::vector<double>> x(N, std::vector<double> (dim, 0));
    std::vector<std::vector<double>> v(N, std::vector<double> (dim, 0));
    std::vector<std::vector<double>> a(N, std::vector<double> (dim, 0));
    std::vector<std::vector<double>> f(N, std::vector<double> (dim, 0));

    // Initial Printout
    //
    std::cout << "\n=== RIEPILOGO === " << std::endl;
    std::cout << "Numero di passi: " << Nsteps << " con timestep: " << deltat << " per un tempo totale di: " << Nsteps*deltat << std::endl;
    std::cout << "Ci sono " << N << " particelle e la densità è " << rho_req << "." << std::endl;

    initialize(N, dim, dx, &x, &v, &a);
//    for (int b=0; b<N; b++) {
//        std::cout << x[b][1] << " ";
//    }
//    std::cout << "\n\n";
    for (s=0; s<Nsteps; s++) {
        update(&x, &v, &a, &f , &r, &kin, &pot);
    }
//    for (int b=0; b<N; b++) {
//        std::cout << x[b][1] << " ";
//    }
//    std::cout << std::endl;

    return 0;
}

void initialize (int, int, double dx, std::vector<std::vector<double>>* x, std::vector<std::vector<double>>* v, std::vector<std::vector<double>>* a) {
    int i, j, k;
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> d{0, T};

    // Initialize positions, velocities and accelerations
    for (i=0; i<N; i++) {
        (*x)[i][0] = (double)(i/(n*n))*dx;
        (*x)[i][1] = (double)(i%(n*n)/n)*dx;
        (*x)[i][2] = (double)(i%(n*n)%n)*dx;
        for (j=0; j<dim; j++) {
            (*v)[i][j] = d(gen);
            (*a)[i][j] = 0.;
        }
    }
}

void update (std::vector<std::vector<double>>* x, std::vector<std::vector<double>>* v, std::vector<std::vector<double>>* a, std::vector<std::vector<double>>* f, std::vector<std::vector<double>>* r, double* kin, double* pot) {
    (*r).clear();
    int i, j, k;
    double potential=0.0, kinetic=0.0;
    // apply periodic border condition
    for (i=0; i<N; i++) {
        for (j=0; j<dim; j++) {
            if ((*x)[i][j] > L) {
                (*x)[i][j] -= L;
            }
            if ((*x)[i][j] < 0) {
                (*x)[i][j] += L;
            }
        }
    }

    // calculate force for every distance
    for (j=0; j<N; j++) {
        for (i=0; i<j; i++) {
            if (i!=j) {
                rij_arr = { (*x)[j][0] - (*x)[i][0] - L*rint(((*x)[j][0] - (*x)[i][0])/L), (*x)[j][1] - (*x)[i][1] - L*rint(((*x)[j][1] - (*x)[i][1])/L), (*x)[j][2] - (*x)[i][2] - L*rint(((*x)[j][2] - (*x)[i][2])/L) };
                (*r).push_back(rij_arr);
                double dist = (sqrt( rij_arr[0] * rij_arr[0] + rij_arr[1] * rij_arr[1] + rij_arr[2] * rij_arr[2]));
                potential += lj(dist);
                for (k=0; k<dim; k++) {
//                    (*f)[j][k] -= derivative(rij_arr[k], PREC, lj);
                    std::cout << ljf(rij_arr[k]) << " ";
                    (*f)[j][k] -= ljf(rij_arr[k]);
                }
            }
        }
        for (k=0; k<dim; k++) { kinetic += .5*(*v)[j][k]*(*v)[j][k]; };
    }
    // velocity verlet
    for (i=0; i<N; i++) {
        for (j=0; j<dim; j++) {
            (*a)[i][j] = (*f)[i][j];
            (*x)[i][j] += (*v)[i][j] * deltat + .5*(*f)[i][j]*deltat*deltat;
            (*v)[i][j] += .5*(*f)[i][j]*deltat;
        }
    }
    std::cout << " potential: " << potential << " kinetic: " << kinetic << std::endl;
//    std::cout << "rows: " << (*r).size() << " cols: " << (*r)[0].size() << std::endl;
}

void refold (std::vector<std::vector<double>>* x) {
    // born-von karmann periodic conditions
    int i, j;
    for (i=0; i<N; i++) {
        for (j=0; j<dim; j++) {
            if ((*x)[i][j] > L) {(*x)[i][j] -= L;}
            if ((*x)[i][j] < 0) {(*x)[i][j] += L;}
        }
    }
}

double lj (double r) { 
    return 4*pow(r, -12.) - 4*pow(r,-6.);
}

double ljf (double r) { 
    return 24.*pow(fabs(r), -7.) - 48.*pow(fabs(r), -13.);
}

double derivative (double x, double h, double func(double x)) {
    return (func(x-2.*h) - 8.*func(x-h) + 8.*func(x+h) - func(x+2.*h) )/(12.*h);
}
