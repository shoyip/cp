#include <iostream>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <time.h>

double PI = 3.14159265359;
double kB = 1.38064852e-23;
double R = 8.3145;

double boxmuller (double mu, double sigma) {
    static const double epsilon = std::numeric_limits<double>::min();
    thread_local double z1;
    thread_local bool generate;
    generate = !generate;

    if (!generate)
        return z1*sigma+mu;

    double u1, u2, z0;
    do {
        u1 = rand() * (1. / RAND_MAX);
        u2 = rand() * (1. / RAND_MAX);
    } while (u1 <= epsilon);
    z0 = sqrt(-2. * log(u1)) * cos(2.*PI * u2);
    z1 = sqrt(-2. * log(u1)) * cos(2.*PI * u2);
    return z0*sigma+mu;
}

int main() {
    srand(time(NULL));
    double m_amu = 131.293;
    double m = m_amu*1.6654e-27;
    double T = 300.;
    double vx, vy, vz;
    double v, v_avg, v_sum=0.;
    int samples = 30;
    double v_vec[samples];
    double v_sigma = sqrt(kB*T/m);

    // Using c++ library
    /*
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> d{0, kB*T/m};
    */

    // Using explicit Box-Muller
    for (int i=0; i<samples; i++) {
        for (int j=0; j<3; j++) {
//            vx = d(gen);
//            vy = d(gen);
//            vz = d(gen);
              vx = boxmuller(0, v_sigma);
              vy = boxmuller(0, v_sigma);
              vz = boxmuller(0, v_sigma);
        }
        std::cout << "vx=" << vx << " vy=" << vy << " vz=" << vz << std::endl;
        v = sqrt(vx*vx + vy*vy + vz*vz);
        std::cout << "v=" << v << std::endl;
        v_vec[i] = v;
    }
    for (int i=0; i<samples; i++) { v_sum += v_vec[i]; }
    v_avg = v_sum / (double) samples;
    std::cout << "The average speed of Xenon atoms is " << v_avg << " m/s." << std::endl;
    std::cout << "From gas law we expect it to be " << sqrt(3.*kB*T/m) << " m/s" << std::endl;
}
