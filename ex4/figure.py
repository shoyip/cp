import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt('plote.dat', delimiter=',')
x = data[:,0]
v = data[:,1]
t =data [:,2]
et = data[:,3]

v, = plt.plot(x,v)
t, =plt.plot(x,t)
et, =plt.plot(x,et)
plt.legend([v,t,et],['V', 'T', 'ETOT'])
plt.show()