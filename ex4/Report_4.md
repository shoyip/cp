---
documentclass: article
classoption: a4paper
lang: it-IT
numbersections: true
title: Simulazione di un box di particelle
author: Matteo Bettanin \and Shoichi Yip
affiliation: Università di Trento
nocite: |
	@*
header-includes: |
	\usepackage{fvextra}
	\DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
	\usepackage{siunitx}
	\usepackage{mathptmx}
	\usepackage{float}
	\usepackage{graphicx}
	\usepackage{physics}
	\usepackage{caption}
abstract: |
	Gli obiettivi di questa esercitazione sono quelli di utilizzare 
	gli strumenti della dinamica molecolare per simulare un sistema di $N$ particelle interagenti 
	tramite \emph{potenziale di Lennard-Jones}, facendo uso dell'algoritmo \emph{velocity verlet} per 
	determinare l'evoluzione temporale del sistema. In primo luogo calcoliamo 
	l'evoluzione nel tempo dell'energia totale, successivamente studiamo la funzione 
	di correlazione delle velocità. Nello specifico, la simulazione che effettuiamo riguarda
    un gas di Xenon.
...

# Introduzione

![Modello del lattice cubico primitivo](lattice.pdf)

In questa esercitazione simuliamo la dinamica molecolare di particelle sferiche
che soddisfano le equazioni del moto di Newton. Per un sistema di un corpo o di
due corpi riusciamo a risolvere le equazioni del moto in modo analitico, mentre
per sistemi costituiti da molteplici particelle dobbiamo ricorrere ad una
soluzione numerica per tener conto di tutte le interazioni.

## Condizioni iniziali

Nel nostro caso scegliamo di disporre le particelle su un reticolo cubico di
lato $L$ con modello *cubico primitivo*, ovvero con un punto per ciascun centro
dell'elemento cubico di lato $\dd{x}$.

Inoltre inizializziamo le velocità in modo randomico secondo la distribuzione di
Maxwell-Boltzmann. Se si suppone l'isotropia del sistema, ovvero che il sistema
non abbia direzioni preferenziali per il moto delle particelle, la distribuzione
di probabilità delle velocità la possiamo scrivere come
$$
f(\vec v) \, \dd[3] \vec r\, \dd[3] \vec v = \left( \frac{m}{2\pi k_B T} \right)^{\frac{3}{2}} \exp[\frac{-mv^2}{2k_B T}]\, \dd[3] \vec r\, \dd[3] \vec v
$$
dove per ciascuna componente vale
$$
f(v_i) \,\dd v_i = \frac{1}{\sqrt{2\pi}\sigma} \exp[\frac{-v_i^2}{2\sigma^2}]\, \dd v_i
$$
che corrisponde alla gaussiana centrata in 0 con $\sigma^2=k_B T/m$, dove $i=x,
y, z$.

## Interazioni ed evoluzione del sistema

Assumiamo come unica interazione tra le particelle il *potenziale di Lennard-Jones*
che ha forma 
$$
	u(r_{ij}) = 4\epsilon \left[ \left(\frac{\sigma}{r_{ij}}\right)^{12}-\left(\frac{\sigma}{r_{ij}}\right)^6\right].
$$

Notiamo come dobbiamo trovare, per impiegare l'equazione di Newton $m_i \dv[2]{\vec r_i}{t} = \vec F_i$, una stima numerica del termine di accelerazione. Dobbiamo
trovarne dunque una forma algebrica che possa fare al nostro caso.  Allora
sviluppiamo la posizione in serie di Taylor *forward*
$$
x(t+h) = x(t) + h \dv{x(t)}{t} + \frac{1}{2} h^2 \dv[2]{x(t)}{t} +
\mathcal{O}(h^3),
$$
e *backwards*
$$
x(t-h) = x(t) - h \dv{x(t)}{t} + \frac{1}{2} h^2 \dv[2]{x(t)}{t} -
\mathcal{O}(h^3).
$$
A questo punto semplifichiamo il termine al prim'ordine degli sviluppi e
otteniamo la cosiddetta *central difference approximation*
$$
\dv[2]{x(t)}{t} = \frac{x(t+h -2 x(t) + x(t-h))}{h^2}
$$
che possiamo inserire nelle equazioni del moto. Per integrare le equazioni del
moto abbiamo tre schemi: il metodo *Verlet*, il metodo *leapfrog Verlet* e il
metodo *velocity Verlet*.

![(a) Verlet (b) Leapfrog Verlet (c) Velocity Verlet](scheme.pdf)

Noi adottiamo l'ultimo schema, poiché ci permette di avere la posizione, la
velocità e l'accelerazione delle particelle contemporaneamente evolute per ogni
istante di tempo. L'uso dello schema di *Verlet* non è preferibile in quanto
l'evoluzione non prende in considerazione le velocità e quindi non permette la
termalizzazione del sistema. Lo schema *leapfrog* è praticamente equivalente
allo schema *velocity Verlet* e prevede uno step in meno. Tuttavia con questo
approccio si calcolano le velocità come la media delle velocità precedenti e
seguenti. Questo porterebbe ad un risultato meno accurato.

Le equazioni di Verlet sono quindi
$$
	\vec{x}(t+\delta t) = \vec{x}(t) + \vec{v}(t)\, \delta t +\frac{1}{2}\vec{a}(t)\delta t^2
$$

$$ 
	\vec{v}(t +\delta t) = \vec{v}(t) + \frac{a(t) + a(t + \delta t)}{2}\delta t
$$
in cui $\vec{a} = \vec{f} / m$ è calcolata a partire dalla forza dovuta al potenziale di Lennard-Jones come
$$
    \vec{f} = - \pdv{u}{r} \, \frac{\vec r_{ij}}{|\vec{r_{ij}}|},
$$
dove $u$ è il potenziale di Lennard-Jones per ciascuna coppia di particelle e
$\vec r_{ij} = \vec r_j - \vec r_i$.

## Disposizione dei box e condizioni di Born-von Karman

Poiché vogliamo studiare la fisica di una porzione di un sistema infinito (non
prendiamo in considerazione il caso delle pareti elastiche), dobbiamo porre
alcune ulteriori condizioni sul moto delle particelle.

Prima di tutto modellizziamo il sistema preso in considerazione come uno delle
infinite copie del *box* che compongono il sistema totale. Richiediamo dunque
che ci sia continuità nelle traiettorie delle particelle che raggiungono le
pareti dei nostri *box*.

Inoltre includiamo questa considerazione nel calcolo delle interazioni
stabilendo una *cutoff distance* che comprenda anche le particelle esterne alla
*box* studiata, ma prossime alla particella nelle *box* adiacenti.

Ricapitolando, fissate le condizioni iniziali iteriamo il seguente algoritmo sulle $N$ particelle:

1. calcoliamo le interazioni reciproche e l'accelerazione $\vec a_{ij}$, tenendo
   conto delle interazioni dovute alle particelle nei *box* adiacenti che
rientrino nella cosidetta *cutoff distance*;
2. aggiorniamo la posizione con le equazioni di *velocity Verlet*;
3. verifichiamo le condizioni di bordo di Born-von Karman;
3. ricalcoliamo le interazioni date le nuove posizioni;
4. aggiorniamo le velocità.

## Energia cinetica ed energia potenziale

Il valore dell'energia potenziale è definito come 
la somma dei valori dei potenziali di interazione tra tutte le particelle.
$$
	V = \sum_{i=1} ^N \sum_{j<i} u(r_{ij})
$$

Ci interessa lo studio di questa grandezza in funzione del tempo per verificare
la effettiva termalizzazione del sistema studiato.

L'energia cinetica $K$ è definita come la somma delle energie cinetiche delle singole particelle
e l'energia totale è $E = K + V$. Ci aspettiamo che l'energia cinetica $K$ sia
tale da garantire la conservazione dell'energia totale $E$ nel tempo.

## Unità adimensionali

Poiché utilizziamo il potenziale di Lennard-Jones, normalizziamo le altre
grandezze per i parametri del potenziale, la $\sigma$ che rappresenta il
raggio della particella, la $\varepsilon$ che rappresenta il minimo del
potenziale, e la massa rappresentativa $m$.

# Implementazione

Abbiamo scritto un programma che simulasse l'evoluzione nel tempo di un sistema
caratterizzato dai seguenti parametri, ovvero un gas di Xenon:

| Parametro     | Valore                        |
|---------------:|-------------------------------|
| $\epsilon$    | \SI{0.02}{\electronvolt}      |
| $\sigma$      | \SI{3.94}{\angstrom}          |
| $\delta t$    | \SI{1}{\pico\second}          |
| $T$           | \SI{300}{\kelvin}             |
| $m$           | \SI{2.2e-22}{kg}              |
| $n$           | 5                             |

Per quanto riguarda la posizione iniziale delle particelle abbiamo creato un
reticolo tridimensionale a partire dal lato della box, e inizializzato con tre
cicli concatenati le particelle al centro dei cubetti componenti il reticolo.
Le velocità invece sono inizializzate secondo una distribuzione di
Maxwell-Boltzmann, che è equivalente a tre distrubuzioni gaussiane, una per
ogni componente della velocità, con media 0 e varianza $k_b T/m$, resa
unitaria dalla scelta delle unità adimensionali.  Per poter avere dei numeri
randomici abbiamo implementato nel programma la libreria `random` e
utilizzato la *box muller transform* per estrarre un valore gaussiano casuale a
partire da un valore casuale compreso tra 0 e 1

$$
 x = \sqrt{-2\ln(S_1)}\cos(2\pi S_2)
$$

con $S_1$ e $S_2$ numeri estratti casualmente tra 0 e 1.

Prima di iniziare i passi di evoluzione si calcolano le interazioni a tempo
zero, per eseguire questo calcolo tenendo conto della _cutoff distance_
utilizziamo la funzione `rint` della libreria `cmath`, la quale arrotonda
all'intero più vicino l'argomento. Così facendo riusciamo a tenere conto del
fatto che in realtà il nostro sistema è inserito in un sistema più grande.

Una volta preparate le condizioni iniziali prende avvio la simulazione, che procede di
\SI{0.1}{\pico\second} per ciclo. Per ogni istante si calcolano le nuove posizioni con
l'algoritmo *velocity Verlet* e si controlla che siano verificate le condizioni
periodiche con la funzione `concon`.  Se una delle variabili che descrivono la
posizione della particella dovesse superare le dimensioni della scatola o
assumere valori negativi, rispettivamente si sottrae o si aggiunge $L$ al valore
della variabile. In questo modo reimmettiamo la
particella dalla parte opposta del cubo. Con le nuove posizioni si ricalcolano
le interazioni, necessarie per trovare le velocità. A questo punto il ciclo
ricomincia daccapo.

Le energie, definita come sopra, vengono salvate ad ogni iterazione del ciclo e
azzerate all' inizio del successivo.  Come si può vedere dal grafico le energie
cinetica e potenziale evolvono dal valore iniziale per poi oscillare intorno ad
un valore di equilibrio. L'energia totale, dopo il primo periodo di
assestamento, rimane costante nel tempo.

![Plot delle energie](giusto_energia.jpeg)


Il grafico mostra bene la riuscita della simulazione: non sono infatti presenti
fluttuazioni dell'energia totale, in accordo con la conservazione dell'energia
per il nostro sistema isolato.  Nell'ultima parte del programma, quando il sistema
ha raggiunto la termalizzazione, calcoliamo la
funzione di correlazione per le velocità, definita come:
 
$$
	f_{\text{corr}} = \frac{\expval{\vec{v}_i(t)\cdot \vec{v}_i(0)}}{\expval{\vec{v}_i(0)}}
$$

Questa funzione che misura la correlazione mediata tra le particelle fornisce un 
informazione sul grado di memoria del sistema. Ci aspettiamo che, essendo il sistema
termalizzato, la correlazione tra la velocità in un dato momento e la velocittà negli 
istanti successivi 
sia caratterizato da un fenomeno transiente per poi stabilizzarsi attorno allo zero,
come si vede in figura.
Il calcolo è eseguito sugli ultimi 5000 punti dell'evoluzione temporale per avere
la conferma che il sistema avesse raggiunto la termalizzazione.

![Funzione di correlazione](corr5000.png)

Si può osservare che la funzione presa in considerazione diminuisce per un certo
numero di passi, per poi fluttuare attorno allo zero.

# Listato del programma

Il programma compilato da `prcc.cpp` genera le condizioni iniziali e sviluppa la
dinamica del sistema salvando in due files distinti i valori delle energie e per
gli ultimi 5000 punti la funzione di correlazione. Per cercare di snellire
l'aspetto del programma abbiamo utilizzato varie funzioni per compiere calcoli
come il potenziale o la forza tra le particelle.

Il tempo di esecuzione è di circa un'ora. 

```cpp
#include <iostream>
#include <cmath>
#include <fstream>
#include <random>

const int steps = 1e6;
const double kb = 8.6173e-5; // eV K^-1
const double eps = 0.02; // eV
const double sigma = 3.94; // Angstrom
const double T = 300*kb/eps; // K
const double m = 2.2e-25;// kg
const int n = 5;
const double e = 1.6021e-19;
const double L = 50/sigma;
const double delt = 1.e-4*sqrt(eps*e / m / pow(sigma, 2)); 
const double dx = L/n;
const double dvar = sqrt(T);

struct part{double x,y,z,vx,vy,vz,ax,ay,az;};
double E, V, K;
void concon(double x);
double ljf(double r);
double lj(double r);
double mbd(double var);

int main(){
    int maxp = pow(n,3);
    int c = 0;
    int cor = 0;
    int cr = 0;
    double x[maxp];
    double y[maxp];
    double z[maxp];
    double vx[maxp];
    double vy[maxp];
    double vz[maxp];
    double ax[maxp];
    double ay[maxp];
    double az[maxp];
    double v0x[maxp];
    double v0y[maxp];
    double v0z[maxp];
    double fc_num, fc_den;
    double fc[5001];
    double xx,yy,zz;

    // impostazione della griglia delle posizioni
    for(int i=0;i<n; i++){
		for(int j=0;j<n; j++){
			for(int k=0;k<n; k++){    
			    x[c] = i*dx + 0.5*dx;
			    y[c] = j*dx + 0.5*dx;
			    z[c] = k*dx + 0.5*dx;
                c++;
			}
		}
	}

    // inizializzazione delle velocità iniziali secondo la distribuzione di
    // Maxwell-Boltzmann
	for(int i = 0; i<maxp ; i++){
		vx[i] = mbd(dvar);
		vy[i] = mbd(dvar);
		vz[i] = mbd(dvar);
        K += .5*(pow(vx[i], 2)+ pow(vy[i],2)+ pow(vz[i], 2));
	}

    // calcolo delle forze al tempo 0
    for(int i=0; i<maxp; i++){
        for(int j=0; j<maxp; j++){
            if(i != j){    
                xx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                yy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                zz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                double dis = sqrt(pow(xx,2) + pow(yy,2)+pow(zz,2));
                ax[i] -= ljf(dis)/dis * xx; 
                ay[i] -= ljf(dis)/dis * yy;
                az[i] -= ljf(dis)/dis * zz;
                V += lj(dis);
            }
        }
    }
    E = K + V;

    // evoluzione temporale con velocity verlet
    std::ofstream corr;
    std::ofstream energy;
    corr.open("fcorr.dat");
    energy.open("energy.dat");

    for(int t=0; t<steps; t++){
        double et = t*delt;
        energy<<et<<","<<V<<","<<K<<","<<E<<std::endl;
        // inizializzazione energie a 0;
        E=0.;
        V=0.;
        K=0.;
        // posizioni a t+dt e primo calcolo delle velocità
        for(int i=0; i<maxp; i++){
            x[i] += vx[i]*delt + 0.5*ax[i]*pow(delt,2); 
            y[i] += vy[i]*delt + 0.5*ay[i]*pow(delt,2); 
            z[i] += vz[i]*delt + 0.5*az[i]*pow(delt,2); 
            vx[i] += 0.5*ax[i]*delt;
            vy[i] += 0.5*ay[i]*delt;
            vz[i] += 0.5*az[i]*delt;
            concon(x[i]);
            concon(y[i]);
            concon(z[i]);
        }

        // aggiornamento delle nuove interazioni        
        for(int i=0; i<maxp; i++){
            ax[i] = 0;
            ay[i] = 0;
            az[i] = 0;
            for(int j=0; j<maxp; j++){
                if(i != j){    
                    xx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                    yy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                    zz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                    double dis = sqrt(pow(xx,2) + pow(yy,2)+pow(zz,2));
                    ax[i] -= ljf(dis)/dis * xx; 
                    ay[i] -= ljf(dis)/dis * yy;
                    az[i] -= ljf(dis)/dis * zz;
                    V += lj(dis)/2;
                }
            }
        }
        for(int i=0; i<maxp; i++){
            vx[i] += 0.5*ax[i]*delt;
            vy[i] += 0.5*ay[i]*delt;
            vz[i] += 0.5*az[i]*delt;
            K += 0.5*(pow(vx[i], 2)+pow(vy[i], 2) + pow(vz[i], 2));
        }
        E = K+V;

        // calcolo della funzione di correlazione delle velocità
        if(t>steps-5002){
            fc_num = 0.;
            fc_den = 0.;
            for (int k=0; k<maxp; k++) {
                if(t==steps-5001){v0x[k] += vx[k]; v0y[k] += vy[k]; v0z[k] += vz[k];};
                fc_num += v0x[k]*vx[k] + v0y[k]*vy[k] + v0z[k]*vz[k];
                fc_den += v0x[k]*v0x[k] + v0y[k]*v0y[k] + v0z[k]*v0z[k];
            }
            fc[t+5001-steps] = fc_num / fc_den;
            corr<<fc[t+5001-steps]<<std::endl;              
        }
    }
    corr.close();
    energy.close();
    return 0;
}

// condizioni al contorno 
void concon(double x){
    if(x<0){
        x += L;
    }
    if(x>L){
        x -= L;
    }
}

// generatore di numeri casuali gaussiani
double mbd(double var){
  double u,v,x;
  double pi = 3.1415926535;
  u = (double)rand() / RAND_MAX;
  v = (double)rand() / RAND_MAX;
  x = sqrt(-2*pow(var,2)*log(u))*cos(2*pi*v);
  return x;
}

// funzione per la forza data dal potenziale di LJ
double ljf(double r){
    if (r == 0){
        return 0;
    } else {
        return 24*(pow(r, -7)-2*pow(r,-13));
    }
}

// funzione per il potenziale LJ
double lj(double r){
	if (r == 0){
		return 0;
	} else {
		return 4*(pow(1/r,12)-pow(1/r, 6));
	}
	
}
```
Per la produzione delle figure ci siamo affidati ad uno script python:

```python
import matplotlib.pyplot as plt
import numpy as np
import math 

data = np.loadtxt('energy.dat', delimiter=',')
x = data[:,0]
v = data[:,1]
t =data [:,2]
et = data[:,3]


v, = plt.plot(x,v)
t, = plt.plot(x,t)
et, = plt.plot(x,et)
plt.legend([v,t,et],['V', 'T', 'ETOT'], loc='center right')
plt.show()


fc5 = np.loadtxt('fcorr.dat')
tc5 = []
for i in range (5001):
    tc5.append(i+1)
    
plt.plot(tc5, fc5)
plt.legend(['correlation'], loc='upper right')
plt.show()

```

# Riferimenti Bibliografici
