#include<iostream>
#include<cmath>
#include<fstream>
#include<random>

const int steps = 1e6;
const double kb = 8.6173e-5; // eV K^-1
const double eps = 0.02; // eV
const double sigma = 3.94; //Angstrom
const double T = 300*kb/eps; //K
const double m = 2.2e-25;//kg
const int n = 5;
const double e = 1.6021e-19;
const double L = 50/sigma;;
const double delt = 1.e-4*sqrt(eps*e / m / pow(sigma, 2)); 
const double dx = L/n;
const double dvar = sqrt(T);

struct part{double x,y,z,vx,vy,vz,ax,ay,az;};
double E,V,K;
void concon(double x);
double ljf(double r);
double lj(double r);
double mbd(double var);

int main(){
    int maxp = pow(n,3);
    int c = 0;
    int cor = 0;
    int cr = 0;
    double x[maxp];
    double y[maxp];
    double z[maxp];
    double vx[maxp];
    double vy[maxp];
    double vz[maxp];
    double ax[maxp];
    double ay[maxp];
    double az[maxp];
    double v0x[maxp];
    double v0y[maxp];
    double v0z[maxp];
    double fc_num, fc_den;
    double fc[5001];
    double xx,yy,zz;

    //griglia posizioni
    for(int i=0;i<n; i++){
		for(int j=0;j<n; j++){
			for(int k=0;k<n; k++){    
			    x[c] = i*dx + 0.5*dx;
			    y[c] = j*dx + 0.5*dx;
			    z[c] = k*dx + 0.5*dx;
                c++;
			}
		}
	}

     //inizializzo le velocità iniziali secondo boltzmann
	for(int i = 0; i<maxp ; i++){
		vx[i] = mbd(dvar);
		vy[i] = mbd(dvar);
		vz[i] = mbd(dvar);
        K += .5*(pow(vx[i], 2)+ pow(vy[i],2)+ pow(vz[i], 2));
	}

     //forze instante 0
    for(int i=0; i<maxp; i++){
        for(int j=0; j<maxp; j++){
            if(i != j){    
                xx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                yy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                zz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                double dis = sqrt(pow(xx,2) + pow(yy,2)+pow(zz,2));
                ax[i] -= ljf(dis)/dis * xx; 
                ay[i] -= ljf(dis)/dis * yy;
                az[i] -= ljf(dis)/dis * zz;
                V += lj(dis);
            }
        }
    }
    E = K + V;

    //evoluzione temporale con velocity verlet
    std::ofstream corr;
    std::ofstream energy;
    corr.open("fcorr.dat");
    energy.open("energy.dat");

    for(int t=0; t<steps; t++){
        double et = t*delt;
        energy<<et<<","<<V<<","<<K<<","<<E<<std::endl;
        //inizializzo energie a 0;
        E=0;
        V=0;
        K=0;
        //posizioni a t+dt e parte della velocità
        for(int i=0; i<maxp; i++){
            x[i] += vx[i]*delt + 0.5*ax[i]*pow(delt,2); 
            y[i] += vy[i]*delt + 0.5*ay[i]*pow(delt,2); 
            z[i] += vz[i]*delt + 0.5*az[i]*pow(delt,2); 
            vx[i] += 0.5*ax[i]*delt;
            vy[i] += 0.5*ay[i]*delt;
            vz[i] += 0.5*az[i]*delt;
            concon(x[i]);
            concon(y[i]);
            concon(z[i]);
        }
        //nuove interazioni
        
        for(int i=0; i<maxp; i++){
            ax[i] = 0;
            ay[i] = 0;
            az[i] = 0;
            for(int j=0; j<maxp; j++){
                if(i != j){    
                    xx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                    yy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                    zz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                    double dis = sqrt(pow(xx,2) + pow(yy,2)+pow(zz,2));
                    ax[i] -= ljf(dis)/dis * xx; 
                    ay[i] -= ljf(dis)/dis * yy;
                    az[i] -= ljf(dis)/dis * zz;
                    V += lj(dis)/2;
                }
            }
        }
        for(int i=0; i<maxp; i++){
            vx[i] += 0.5*ax[i]*delt;
            vy[i] += 0.5*ay[i]*delt;
            vz[i] += 0.5*az[i]*delt;
            K += 0.5*(pow(vx[i], 2)+pow(vy[i], 2) + pow(vz[i], 2));
        }
        E = K+V;

        // correlazione delle velocità
        if(t>steps-5002){
            
            fc_num = 0.;
            fc_den = 0.;
            
            for (int k=0; k<maxp; k++) {
                if(t==steps-5001){v0x[k] += vx[k]; v0y[k] += vy[k]; v0z[k] += vz[k];};
                fc_num += v0x[k]*vx[k] + v0y[k]*vy[k] + v0z[k]*vz[k];
                fc_den += v0x[k]*v0x[k] + v0y[k]*v0y[k] + v0z[k]*v0z[k];
            }
            fc[t+5001-steps] = fc_num / fc_den;
            corr<<fc[t+5001-steps]<<std::endl;              
            

        }


       

    }
    corr.close();
    energy.close();
    return 0;
}

//condizioni al contorno 
void concon(double x){
    if(x<0){
        x += L;
    }
    if(x>L){
        x -= L;
    }
}
//funzione per numeri casuali dalla gaussiana
double mbd(double var){
  double u,v,x;
  double pi = 3.1415926535;
  u = (double)rand() / RAND_MAX;
  v = (double)rand() / RAND_MAX;

  x = sqrt(-2*pow(var,2)*log(1-u))*cos(2*pi*v);
  return x;
}
//funzione per la forza
double ljf(double r){
    if (r == 0){
        return 0;
    }else{
        return 24*(pow(r, -7)-2*pow(r,-13));
    }
}
//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*(pow(1/r,12)-pow(1/r, 6));
	}
	
}
