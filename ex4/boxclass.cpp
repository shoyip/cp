#include<iostream>
#include<cmath>
#include<fstream>
#include<random>

double steps = 1e6;
double kb = 1.380649e-23;
double eps = 1;//3.20435313e-21; //J
double sigma = 1;//3.94e-10; //m
double delt = 1e-14; //s
double T = 300; //K
int m = 1;//2.2e-25;
int n = 5;
double dx = 1e-9;
double L = n*dx;
int j = 0;
int c = 0;
double dvar = sqrt(kb*T/m);
class part{
     public:
    double x,y,z,vx,vy,vz,fx,fy,fz,ax,ay,az;
        void PosEvo(void){
            x += vx*delt + .5*ax*pow(delt, 2);
            y += vy*delt + .5*ay*pow(delt, 2);
            z += vz*delt + .5*az*pow(delt, 2);
        };
        void VelEvo(double a[3]){
            vx += .5*(a[0]+ ax)*delt;
            vy += .5*(a[1]+ ay)*delt;
            vz += .5*(a[2]+ az )*delt;
        };
};
double phi, theta;
std::random_device rd;
std::mt19937 gen(rd());
void acc(part p);
double lj(double r);
double ljf(double r);
double rad(part p1, part p2);
void concon(part p);
std::ofstream ofile;


int main(){
    int maxp = pow(n,3);
    double a[3];
    double r[maxp][maxp];
    class part p[maxp];
    //griglia posizioni
    for(int i=0;i<n; i++){
		for(int j=0;j<n; j++){
			for(int k=0;k<n; k++){    
			    p[c].x = i*dx + .5*dx;
			    p[c].y = j*dx + .5*dx;
			    p[c].z = k*dx + .5*dx;
			    c++;
            //std::cout<<p[c].x<<"	"<<p[c].y<<"	"<<p[c].z<<std::endl;
			}
		}
	}
    
    //velocità 
    //inizializzo le velocità iniziali secondo boltzmann
	
	for(int i = 0; i<maxp ; i++){
		std::normal_distribution<> d(0, 1); //dvar
		p[i].vx = d(gen);
		p[i].vy = d(gen);
		p[i].vz = d(gen);
	}
    //distanze tempo 0
    for(int i=0; i<maxp; i++){
        j = 0;
        while(j<i){
            r[i][j] = rad(p[i], p[j]);
            r[j][i] = -r[i][j];
            j++;
        }
    }
    //forze tempo 0
    for(int i=0; i<maxp; i++){
        for(int j=0; j<maxp; j++){
          if(j!=i){
            phi = acos((p[i].z-p[j].z)/r[i][j]);
			theta = atan2(p[i].y - p[j].y, p[i].x - p[j].x);
            p[i].fx += ljf(r[i][j])*sin(phi)*cos(theta);
			p[i].fy += ljf(r[i][j])*sin(phi)*sin(theta);
			p[i].fz += ljf(r[i][j])*cos(phi);
          }  
        }
    }

    int V = 0;
    int K = 0;
    int E = 0;

    ofile.open("plote.dat");
    //ciclo i tempi
    for(int t=0; t<steps; t++){
        double et = t*delt;

        //calcolo le energie con ovvia notazione
        for(int i=0; i<maxp; i++){
            for(int j=0; j<i; j++){
                V += lj(r[i][j]);
            }
            K += .5*m*(pow(p[i].vx, 2)+pow(p[i].vy, 2)+pow(p[i].vz, 2));
        }
        E = V + K;
    	ofile<<et<<","<<V<<","<<K<<","<<E<<std::endl;
        //nuove posizioni
        for(int i = 0; i<maxp; i++){
            acc(p[i]);
            p[i].PosEvo();
            concon(p[i]);
        }
        //nuove distanze
        for(int i=0; i<maxp; i++){
            j = 0;
            while(j<i){
                r[i][j] = rad(p[i], p[j]);
                r[j][i] = -r[i][j];
                j++;
            }
        }
        //nuove interazioni
        for(int i=0; i<maxp; i++){
            p[i].fx = 0;
            p[i].fy = 0;
            p[i].fz = 0;
            for(int j =0; j<maxp; j++){
                if(j!=i){
                    phi = acos((p[i].z-p[j].z)/r[i][j]);
                    theta = atan2(p[i].y - p[j].y, p[i].x - p[j].x);
                    p[i].fx += ljf(r[i][j])*sin(phi)*cos(theta);
                    p[i].fy += ljf(r[i][j])*sin(phi)*sin(theta);
                    p[i].fz += ljf(r[i][j])*cos(phi);
                } 
            }
        }
        //a(t+dt) e v(t+dt)
        for(int i =0; i<maxp; i++){
            a[0] = p[i].fx/m;
			a[1] = p[i].fy/m;
			a[2] = p[i].fz/m;
			p[i].VelEvo(a);
        }
    
        V = 0;
        K = 0;
        E = 0;

        /*
		if(steps-t==500){
			for(int k =0; k<maxp; k++){
				v0[k] = sqrt(pow(p[i].vx,2) + pow(p[i].vy, 2) + pow(p[i].vz,2));
				v02[k] = pow(p[i].vx,2) + pow(p[i].vy, 2) + pow(p[i].vz,2);
			}
		}
		if(steps-t<501){
			for(int k=0; k<maxp; k++){
				vt[k]=sqrt(pow(p[i].vx,2) + pow(p[i].vy, 2) + pow(p[i].vz,2));
				fcorr[u] += vt[k]*v0[k]/v02[k]; 
			}
			u+=1;
		}
        */
    }
    ofile.close();
    return 0;
}


double rad(part p1, part p2){
    return sqrt(pow(p1.x-p2.x, 2.0)+pow(p1.y-p2.y, 2.0)+pow(p1.z-p2.z, 2.0));
}

//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*eps*(pow(sigma/r,12)-pow(sigma/r, 6));
	}
	
}

//funzione per la forza
double ljf(double r){
    if (r == 0){
        return 0;
    }else{
        return 24*pow(sigma, 6)*(pow(r, -7)-2*pow(sigma/r,13));
    }
}
void concon(part p){
    if(p.x>L){
        p.x-=L;
    }
    if(p.y>L){
        p.y-=L;
    }
    if(p.z>L){
        p.z-=L;
    }
    if(p.x<0){
        p.x += L;
    }
    if(p.y<0){
        p.y += L;
    }
    if(p.z<0){
        p.z+=L;
    }
}
void acc(part p){
    p.ax = p.fx/m;
    p.ay = p.fy/m;
    p.az = p.fz/m;
}