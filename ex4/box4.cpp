#include<iostream>
#include<cmath>
#include<fstream>
#include<random>

const double kb = 8.6173e-5;
double eps = 0.02; //eV
double sigma = 3.94; //1e-10m
double m = 2.2e-25; //Kg
double delt = 1e-14; //s

double T = 300*kb/eps; //K
const int n = 5;

double steps = 1e5;
double rho =  pow(sigma,3)*1e27;
int c = 0;
int i = 0;
double a[3];
double V = 0;
double K = 0; //energia cinetica
double E = 0;
double lj(double r);
double ljf(double r);
int indic = 0;
std::ofstream ofile;

int main(){
	struct part{
		double x,y,z,vx,vy,vz,fx,fy,fz,ax,ay,az;
	};
	struct part p[125];
	struct mirr{
		double x,y,z;
	};
	//struct mirr m[3250];
	double L = 4e-9;
	double dx = L/4;
	double x0[n];
	double y0[n];
	double z0[n];
	double r2[125][125];
	double r[125][125];
	double dvar = sqrt(kb*T/m);
	
	for (int i = 0; i<n; i++){
		x0[i] = i*dx; 
		y0[i] = i*dx;
		z0[i] = i*dx;
	}

	//inizializzo le posizioni su un reticolo.
	for(int i=0;i<n; i++){
		for(int j=0;j<n; j++){
			for(int k=0;k<n; k++){
			p[c].x = x0[i];
			p[c].y = y0[j];
			p[c].z = z0[k];
			//std::cout<<p[c].x<<"	"<<p[c].y<<"	"<<p[c].z<<std::endl;
			c += 1;
			}
		}
	}	
	
	
	//inizializzo le velocità iniziali secondo boltzmann
	std::random_device rd;
	std::mt19937 gen(rd());
	
	for(i = 0; i<125 ; i++){
		std::normal_distribution<> d(0, dvar);
		p[i].vx = d(gen);
		p[i].vy = d(gen);
		p[i].vz = d(gen);
		//std::cout<<p[i].vx<<std::endl;
	}
	
	//calcolo le distanze tra le particelle
	
	for (int i=0; i<125; i++){
		int j = 0;
		while(j<i){
			r2[i][j] = pow(p[i].x-p[j].x, 2.0)+pow(p[i].y-p[j].y, 2.0)+pow(p[i].z-p[j].z, 2.0); 
			r[i][j] = sqrt(r2[i][j]);
			//std::cout<<r[i][j]<<"	";
			r[j][i] = r[i][j];
			j += 1;
			
			}
			//std::cout<<std::endl;
	}
	
	//interazioni al tempo 0:
		double phi, theta;
		for (int i=0; i<125; i++){
			
			for(int j = 0; j<125; j++){
				if(j!=i){
				phi = acos((p[i].z-p[j].z)/r[i][j]);
				theta = atan2(p[i].y - p[j].y, p[i].x - p[j].x);
				p[i].fx += -ljf(r[i][j])*sin(theta)*cos(phi);
				p[i].fy += -ljf(r[i][j])*sin(theta)*sin(phi);
				p[i].fz += -ljf(r[i][j])*cos(phi);
				j += 1;
				
				}
			}
			//std::cout<<phi<<"	"<<theta<<std::endl;
		}
	
		
	//ciclo sui tempi
	ofile.open("etot4.dat");
	for(int t = 0; t<steps; t++){//sarebbe sul tempo
		
		// calcolo V e T
		for(int i=0; i<125; i++){
			for(int j=0; j<i; j++){
				V += lj(r[i][j]);
				//std::cout<<V<<std::endl;
			} 
			K += 0.5*(pow(p[i].vx, 2)+pow(p[i].vy, 2)+pow(p[i].vz, 2));
		}
		E = V+K;
		
		ofile<<t*delt<<"	"<<E<<std::endl;
		
		
		
		//velocity verlet
		for(i= 0; i<125; i++){
			p[i].ax = p[i].fx;
			p[i].ay = p[i].fy;
			p[i].az = p[i].fz;
			
			p[i].x = p[i].x + p[i].vx*delt + 0.5*p[i].ax*pow(delt, 2.0);
			p[i].y = p[i].y + p[i].vy*delt + 0.5*p[i].ay*pow(delt, 2.0);
			p[i].z = p[i].z + p[i].vz*delt + 0.5*p[i].az*pow(delt, 2.0);
			//std::cout<<p[i].x<<std::endl;
		}	
			//condizioni periodiche
		
			for(int i=0;i<125; i++){
				if(p[i].x  > 1.1*L){
					p[i].x -= L;
				}
				if(p[i].y  > 1.1*L){
					p[i].y -= L;
				}
				if(p[i].z  > 1.1*L){
					p[i].z -= L;
				}
				if(p[i].x  < -0.1e-9){
					p[i].x += L;
				}
				if(p[i].y  < -0.1e-9){
					p[i].y += L;
				}
				if(p[i].z  < -0.1e-9){
					p[i].z += L;
				}
		    }	
			//ricalcolo le distanze tra le particelle
	
			for (int i=0; i<125; i++){
				int j = 0;
				while(j<i){
					r2[i][j] = pow(p[i].x-p[j].x, 2.0)+pow(p[i].y-p[j].y, 2.0)+pow(p[i].z-p[j].z, 2.0); 
					r[i][j] = sqrt(r2[i][j]);
					r[j][i] = r[i][j];
					//std::cout<<r[i][j]<<"	";
					j += 1;
			
				}
				//std::cout<<std::endl;
			}
			
			//ricalcolare le interazioni con le nuove posizioni
			for (int i=0; i<125; i++){
				p[i].fx = 0;
				p[i].fy = 0;
				p[i].fz = 0;
				
				for(int j = 0; j<125; j++){
					if(j!=i){
						phi = acos((p[i].z-p[j].z)/r[i][j]);
						theta = atan2(p[i].y - p[j].y, p[i].x - p[j].x);;
						p[i].fx += -ljf(r[i][j])*sin(theta)*cos(phi);
						p[i].fy += -ljf(r[i][j])*sin(theta)*sin(phi);
						p[i].fz += -ljf(r[i][j])*cos(phi);
						j += 1;
					}
				}
			}
			
			for(int i =0; i<125; i++){
			//a(t + dt) e v(t+dt)
			a[0] = p[i].fx;
			a[1] = p[i].fy;
			a[2] = p[i].fz;
			p[i].vx = p[i].vx + 0.5*(a[0] + p[i].ax)*delt;
			p[i].vy = p[i].vy + 0.5*(a[1] + p[i].ay)*delt;
			p[i].vx = p[i].vz + 0.5*(a[2] + p[i].az)*delt;
			}
		
		V = 0;
		E = 0;
		K = 0;
		

	} 	
	
	ofile.close();
	return 0;
}




//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*eps*(pow(sigma/r,12)-pow(sigma/r, 6));
	}
	
}
//funzione per la forza a partire dal potenziale lj.
double ljf(double r){
	return 24*eps*pow(sigma, 6)*(pow(r,-7)-2*pow(sigma,6)*pow(r,-13 ));
	
}

