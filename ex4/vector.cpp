#include <vector>
#include <iostream>

int main() {
    // Array example
    size_t size = 10;
    int sarray[10];
    int *darray = new int[10];
    for (int i=0; i<10; i++) {
        sarray[i] = i;
        darray[i] = i;
    }
    delete [] darray;
    
    // Vector example
    std::vector<int> array;
    array.reserve(30);
    for (int i=0; i<size; i++) {
        array[i] = i;
//        std::cout << array[i] << std::endl;
    }

    std::vector<std::vector<std::vector<double>>> a2 = {{{1,2,3}, {4,5,6}, {7,8,9}}, {{10, 11, 12}, {13, 14, 15}, {16, 17, 18}}, {{19, 20, 21}, {22, 23, 24}, {25, 26, 27}}};
    for (int j=0; j<26; j++) {
        std::cout << a2[j/9][(j%9)/3][(j%9)%3] << std::endl;
    }

    return 0;
}
