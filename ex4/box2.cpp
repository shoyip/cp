#include <iostream>
#include <iomanip>
#include <cmath>

int main ( int argc, char *argv[] );
void compute ( int N, int dim, double p[], double v[], double m, double f[], double &pot, double &kin );
double distance ( int dim, double r1[], double r2[], double dr[] );
void initialize ( int N, int dim, double p[], double v[], double a[]);
void update ( int N, int dim, double p[], double v[], double f[], double a[], double m, double dt );
double boxmuller (double mu, double sigma);

double kB = 1.380649e-23;
double PI = 3.141592653589793;
double T = 300;
double m = 1.;

int main ( int argc, char *argv[] ) {
    // Define variables
    srand(time(NULL));
    double m_amu = 131.293; // Xenon mass in amu
    double m = m_amu*1.6654e-27; // Xenon mass in si
    double T = 300.;
    int dim, N, steps, dt;
    double *p, *v, *a, *f;
    double dt, kin, pot, e0, rho;
    double v_sigma = sqrt(kB*T/m);

    // Assign variables from input
    std::cout << "Inserisci il numero di dimensioni spaziali (2 o 3)" << std::endl;
    std::cin >> dim;
    std::cout << "\nInserisci il numero di particelle (ad es. 125)" << std::endl;
    std::cin >> N;
    std::cout << "\nInserisci la densità (ad es. 10e27)" << std::endl;
    std::cin >> rho;
    std::cout << "\nInserisci il numero di timesteps (ad es. 500)" << std::endl;
    std::cin >> steps;
    std::cout << "\nInserisci la lunghezza del timestep (ad es. 0.1)" << std::endl;
    std::cin >> dt;

    for (int step = 0; step < steps; step++) {
        if (step == 0) {
            initialize (N, dim, p, v, a);
        } /*else {
            update (N, dim, p, v, f, a, m, dt);
        }
        compute(N, dim, p, v, m, f, pot, kin);
        if (step == 0) { e0 = pot + kin; }
        std::cout << setw(8) << step <<
            " " << setw(14) << potential + kinetic << std::endl;
            */
    }

    // Free allocated memory
    delete [] p;
    delete [] v;
    delete [] a;
    delete [] f;

    return 0;
}

/*
void compute ( int N, int dim, double p[], double v[], double m, double f[], double &pot, double &kin ) {
    // Compute forces and energies

    int i, j, k;
    double d, d2;

    pot = 0.0;
    kin = 0.0;

    for (k=0; k<N; k++) {
        for (i=0; i<dim; i++) { f[i+k*dim] = 0.0; }
        for (j=0; j<N; j++) {
            if (k!=j) { d = distance(dim, p+k*dim, p+j*dim, rij); }
            if (d < .5*PI) {
                d2 = d
            } else {
                d2 = .5*PI;
            }
            pot = pot + .5*pow(sin(d2), 2);
            for (i=0; i<dim; i++) { f[i+k*dim] -= rij[i] * sin(2.*d2)/d; }
        }
    }
}
*/
double distance ( int dim, double r1[], double r2[], double dr[] ) {
    double d;
    int i;

    d = 0.0;
    for (i=0; i<dim; i++) {
        dr[i] = r1[i] - r2[i];
        d += dr[i] * dr[i];
    }
    d = sqrt(d);
    return d;
}

void initialize ( int N, int dim, double p[], double v[], double a[]) {
    int i, j;

    // Set positions, velocities and accelerations
    for (j=0; j<N, j++) {
        for (i=0; i<dim; i++) {
            p[i+j*dim] = j*dx;
            v[i+j*dim] = boxmuller(0, v_sigma);
            a[i+j*dim] = 0.0;
        }
    }
}

void update ( int N, int dim, double p[], double v[], double f[], double a[], double m, double dt ) {
    int i, j;
    double rm = 1.0/mass; // Inverted mass

    for (j=0; j<N; j++) {
        for (i=0; i<dim; i++) {
            p[i+j*dim] += v[i+j*dim]*dt + .5*a[i+j*dim]*dt*dt;
            v[i+j*dim] += v[i+j*dim] + .5*(f[i+j*dim]*rm + a[i+j*dim])*dt;
            a[i+j*dim] += f[i+j*dim]*rm;
        }
    }
}

double boxmuller (double mu, double sigma) {
    static const double epsilon = std::numeric_limits<double>::min();
    thread_local double z1;
    thread_local bool generate;
    generate = !generate;

    if (!generate)
        return z1*sigma+mu;

    double u1, u2, z0;
    do {
        u1 = rand() * (1. / RAND_MAX);
        u2 = rand() * (1. / RAND_MAX);
    } while (u1 <= epsilon);
    z0 = sqrt(-2. * log(u1)) * cos(2.*PI * u2);
    z1 = sqrt(-2. * log(u1)) * cos(2.*PI * u2);
    return z0*sigma+mu;
}
