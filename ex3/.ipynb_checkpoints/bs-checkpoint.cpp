#include<iostream>
#include<cmath>
#include<fstream>

const int N = 200;
const int M = 2000;
double T = 0.25;
double E = 10.0;
double r = 0.1;
double sigma = 0.4;
double a = 2*r/pow(sigma, 2) - 1;
double b = -2*r/pow(sigma, 2);
double smax = 30.0;
double delt = T/M;
double dels = smax/(N-1);
double S[N];
double t[M];
double v[M][N];
double alpha = pow(sigma, 2.0)*delt;
double beta = r*delt;
double file[N];
int n = 0, m = 0,i = 0;
std::ofstream ofile;


int main() {
	
	
	
	ofile.open ("dataV.dat");
	while(i<N){
		S[i] = i*dels;
		if (S[i]-E <= 0){
		v[i][0] = 0;
		}
		else {
		v[i][0] = S[i] - E;
		}
		std::cout<<S[i]<<std::endl;	
		i+=1;
	}
	for(m = 0; m < M; m++){
		t[m] = m*delt;
		v[0][m] = 0;
		v[N-1][m] = smax -E;
		for(n = 1; n < N; n++){
			v[n][m+1] = 0.5*(alpha*pow(n,2)-beta*n)*v[n-1][m] + (1-alpha*pow(n,2)- beta)*v[n][m] + 0.5*(alpha*pow(n, 2) + beta*n)*v[n+1][m];
			ofile<< v[n][m]<<"\t";
		}
		ofile<<std::endl;
	}
	ofile.close();
	
	return 0;
}

