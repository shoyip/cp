#include <iostream>
#include <cmath>
#include <fstream>

// Numero di particelle
int N;
// Numero di dimensioni
int dim;

// Parametri Lennard-Jones
double sigma = 1.;
double epsilon = 1.;
double m = 1.;
double kB = 1.;

// Costanti (Avogadro e Boltzmann)
double NA = 6.022140857e23;
double kBSI = 1.38064852e-23;

// Dimensioni della box
double L;

// Temperatura iniziale
double T0;

const int MAXPART = 5001; // Numero massimo particelle

// Vettore Posizione
r = new double[N*dim];
// Vettore Velocità
v = new double[N*dim];
// Vettore Accelerazione
a = new double[N*dim];
// Vettore Forza
F = new double[N*dim];

// Function Prototypes

int main(int argc, char *argv[]) {
    return 0;
}
