unset key
set size ratio 2
set term png
set out "fig3.png"
set xlabel "$t$"
set ylabel "$S$"
set zlabel "$V(S, t)$"
set pm3d
load 'viridis.pal'
splot "prova.dat" with dots palette
