#include <iostream>
#include <cmath>
#include <iomanip>

float T = 0.25; // time of expiry
float E = 10.0; // strike price
float r = 0.1; // rate of interest of risk-free security
float sigma = 0.4; // volatility
float Smax = 3.0*E;

int N = 20;
int M = 20;

float dt = T/(float)M;
float dS = Smax/(float)N;

float alpha = pow(sigma, 2) * dt;
float beta = r * dt;

float S, t;

int main() {
    float vp[N];
    float v[N];
    for (int n=0; n<=N; n++) {
        S = dS*(float)n;
        v[n] = S-E<0 ? 0 : S-E;
        std::cout << v[n] << ", ";
    }
    std::cout << std::endl;
    for (int m=1; m<=M; m++) {
        t = (float)m*dt;
        for (int n=1; n<=N; n++) {
            S = (float)n*dS;
            if (n==0) {
                v[n] = 0;
            } else if (n==N) {
                v[n] = Smax-E;
            } else {
                v[n] = .5 * (alpha*pow((float)n, 2) - beta * (float)n) * vp[n-1] + (1-alpha*pow((float)n, 2) - beta) * vp[n] + .5 * (alpha * pow((float)n, 2) + beta * (float)n) * vp[n+1];
            }
            std::cout << v[n] << ", ";
        }
        for (int i=0; i<N; i++) {
            vp[i] = v[i];
        }
        std::cout << std::endl;
    }
    return 0;
}
