from mpl_toolkits.mplot3d import axes3d
import numpy as np
import matplotlib.pyplot as plt
V1 = np.loadtxt('V1.dat', dtype=None)
t1 = np.linspace(0,0.25,41000)
s1 = np.linspace(0,30.0,1999)
(S1, T1) = np.meshgrid(s1, t1)
fig1 = plt.figure()
ax1 = fig1.gca(projection='3d')
ax1.set_xlabel('stock price $S$')
ax1.set_ylabel('time $t$')
ax1.set_zlabel('option price $V(S,t)$')
surf = ax1.plot_surface(S1, T1, V1, cmap='viridis')
ax1.view_init(elev=30, azim=130)
fig1.savefig('fig2.png')
