---
documentclass: article
classoption: a4paper
lang: it-IT
numbersections: true
title: Metodi Numerici per lo studio dell'equazione di Black-Scholes
author: Matteo Bettanin \and Shoichi Yip
affiliation: Università di Trento
nocite: |
	@*
header-includes: |
	\usepackage{fvextra}
	\DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
	\usepackage{siunitx}
	\usepackage{mathptmx}
	\usepackage{float}
	\usepackage{graphicx}
	\usepackage{physics}
	\usepackage{caption}
abstract: |
	L'obiettivo di questa esercitazione è lo studio dell'\emph{equazione di
	Black-Scholes}. Date le ipotesi teoriche sviluppiamo numericamente
	l'equazione alle derivate parziali su una \emph{European Call Option} per
	diversi valori di $S$ (\emph{stock price}) e per discretizzazioni
	differenti sul tempo $t$ e su $S$. Infine compariamo i risultati
	numerici con quelli analitici dati dall'equazione del calore.
...

# Introduzione

L'_equazione di Black-Scholes_ viene utilizzata nell'ambito della matematica
finanziaria per il _pricing_ dei derivati, ovvero di prodotti finanziari con un
valore che dipende da uno o più _asset_ sottostanti. Il derivato è un contratto
tra parti, e il valore del derivato dipende dalle fluttuazioni dell'_asset_
sottostante. Nel nostro caso prendiamo in considerazione le _European Call
Options_, ovvero delle opzioni in cui si ha il diritto di comprare il
sottostante ad un determinato _strike price_ alla scadenza dell'opzione. Poiché
abbiamo a che fare con delle opzioni _European style_, l'opzione può essere
esercitata alla scadenza.

Dato il prezzo del sottostante $S$, si ipotizza che il suo prezzo segua un _moto
browniano geometrico_, per cui
$$
\dd{S} = \mu S \dd{t} + \sigma S \dd{W},
$$ {#eq:dS}
dove $\mu$ e $\sigma$ sono delle costanti.

Il prezzo dell'opzione è una funzione di due variabili e tre parametri
$$
V = V(T-t, S; r, \sigma, E),
$$ {#eq:V}
dove $T$ è la scadenza, $S$ è il prezzo del sottostante e $E$ è lo _strike
price_.

Per studiare l'evoluzione in un tempo infinitesimo $\dd{t}$ del prezzo
dell'opzione $V$ abbiamo per il _Lemma di Ito_ che
$$
\begin{aligned}
\dd{V} &= \pdv{V}{t} \dd{t} \pdv{V}{S} \dd{S} \frac{1}{2} \pdv[2]{V}{S} \dd{S^2}
\\
&= \left( \pdv{V}{t} + \mu S \pdv{V}{S} + \frac{1}{2} \sigma^2 S^2 \pdv[2]{V}{S}
\right) \dd{t} + \sigma S \pdv{V}{S} \dd{W}
\end{aligned}
$$
dove abbiamo rispettivamente in parentesi il termine di deriva e fuori dalla
parentesi il termine stocastico. Per ridurre il rischio associato alle
fluttuazioni del prezzo del sottostante, nella Black-Scholes si annulla il
termine stocastico sfruttando il fatto che sia l'opzione che il sottostante
dipendono dallo stesso termine di _moto geometrico browniano_. Questa strategia
si chiama _Delta Hedging_.

Un'altra ipotesi è che il tasso di interesse dei _risk-free securities_ che
prendiamo in considerazione (ad esempio, dei _bond_) sia $r$ costante
$$
\dd{B} = r B \dd{t}.
$$

Ora prendiamo le _securities_, il sottostante e l'opzione e inseriamoli in un
portfolio. Facciamo l'assunzione che questo portfolio sia _self-financing_, cioè
che non ci sia nessuna iniezione o esborso di denaro dal portfolio, e che le
transazioni non abbiano alcun costo. Il nostro portfolio è quindi
$$
\Pi = \Delta S + \alpha B + V,
$$ {#eq:portfolio}
dove $\Delta$ e $\alpha$ sono le unità rispettivamente di sottostante e di
_risk-free security_ che inseriamo nel portfolio. La variazione infinitesima
nel tempo $\dd{t}$ di $\Pi$ è
$$
\begin{aligned}
\dd{\Pi} &= \Delta \dd{S} + \alpha \dd{B} + \dd{V}\\
&= (\Delta \mu S + \alpha r B) \dd{t} + \Delta \sigma S \dd{W} + \left(
\pdv{V}{t} + \mu S \pdv{V}{S} + \frac{1}{2} \sigma^2 S^2 \pdv[2]{V}{S} \right)
\dd{t} + \sigma S \pdv{V}{S} \dd{W}\\
\dd{\Pi} &= \left( \pdv{V}{t} + \frac{1}{2} \sigma^2 S^2 \pdv[2]{V}{S} +\alpha r B
\right) \dd{t}
\end{aligned}
$$ {#eq:dportfolio}
dove nell'ultimo passaggio eliminiamo il termine stocastico ponendo $\Delta = 
\pdv{V}{S}$. Poiché i termini rimanenti sono solo termini deterministici,
facciamo anche l'assunzione
che in ogni momento siano estinte possibilità di arbitraggio. Quindi ponendo questa condizione sui
termini in Eq. @eq:portfolio e @eq:dportfolio
$$
\begin{aligned}
\dd{\Pi} &= r \Pi \dd{t}\\
\left( \pdv{V}{t} + \frac{1}{2} \sigma^2 S^2 \pdv[2]{V}{S} +\alpha r B
\right) \dd{t} &= \left(V - \pdv{V}{S} S + \alpha B \right) r \dd{t}
\end{aligned}
$$
Semplificando troviamo l'_equazione di Black-Scholes_
$$
\pdv{V}{t} + \frac{1}{2} \sigma^2 S^2 \pdv[2]{V}{S} + r S \pdv{V}{S} - rV = 0,
$$ {#eq:bs}
che riformulata in termini delle greche si esprime come
$$
\Theta + \frac{1}{2} \sigma^2 S^2 \Gamma = rV - rS\Delta.
$$ {#eq:bs_greeks}

L'osservazione topica di Black e Scholes era quindi che il guadagno _risk-free_
in un tempo infinitesimo $\dd{t}$ di un portfolio costituito da un sottostante e
un derivato, che è il termine a destra, può essere espresso come la somma di
$\Theta$, che rappresenta la variabilità nel tempo del premio di un'opzione, e
di una funzione lineare in $\Gamma$, che invece è la convessità del premio
dell'opzione rispetto al prezzo del sottostante. Poiché $\Theta$ è solitamente
negativa, dato che il valore dell'opzione decresce all'approssimarsi della
scadenza, e $\Gamma$ è positiva, poiché dà una misura del guadagno del portfolio
data l'opzione, l'osservazione di Black-Scholes è detta talvolta _risk-neutral
argument_. Come ci aspettiamo i termini $\Theta$ e $\Gamma$ si controbilanciano, dando
come risultato un tasso di interesse _risk-free_.

# Risoluzione delle PDE Black-Scholes per una European Call Option

Per trovare l'evoluzione nel tempo e per $S$ diversi dell'opzione dobbiamo
intanto eseguire alcune operazioni preliminari. Intanto supponiamo di avere
un'opzione dal valore $V(S, t)$, che dipende solamente dal sottostante  $S$ e
dal tempo $t$. L'equazione alle derivate parziali di Black-Scholes (Eq. @eq:bs)
è
$$
\pdv{V}{t} \/ (S, t) + \frac{1}{2} \sigma^2 S^2 \pdv[2]{V}{S} \/ (S, t) + r S
\pdv{V}{S} \/ (S, t) - rV (S, t) = 0.
$$ {#eq:bs2}

Applichiamo il cambio di variabile
$$
\tau(t) = T - t,
$$
per rendere l'equazione parabolica _backwards_ in una equazione parabolica
_forward_, che sappiamo trattare meglio. Sostituendo nell'Eq. @eq:bs2 otteniamo 
$$
\pdv{V}{\tau} \/ (S, \tau) - \frac{1}{2} \sigma^2 S^2
\pdv[2]{V}{S} \/ (S, \tau) - r S \pdv{V}{S} \/ (S, \tau) + r V (S, \tau) = 0.
$$

Abbiamo quindi la condizione iniziale
$$
V(S, \tau=0) = \max(S-E, 0)
$$
e le seguenti condizioni al contorno
$$
\begin{cases}
V(0, \tau) = 0\\
V(S, \tau)/S \to 1, \quad\text{per }S\to\infty
\end{cases}
$$
nel nostro caso, in cui studiamo una _European Call Option_.
Di seguito ridefiniremo $\tau$ con $t$.

## Dalla Black Scholes all'Equazione del Calore

Data l'Equazione di Black-Scholes come espressa in Eq. @eq:bs, possiamo eseguire
alcune sostituzioni al fine di ottenere l'equazione del calore, rendendo più
semplice il computo analitico. Abbiamo quindi
$$
\tau = \frac{\sigma^2}{2} (T-t) \quad x = \ln(\frac{S}{E}) \quad v = \frac{V}{E}
$$
Inizialmente dividiamo l'intera espressione per E (diverso da 0) e eseguiamo le
sostituzioni, otteniamo:
$$
-\frac{\sigma^2}{2}\pdv{v}{\tau}+\frac{1}{2}\sigma^2 S^2\left(\pdv{}{x}\pdv{x}{S}
 \left(\pdv{v}{x}\pdv{x}{S}
\right)\right)+rS\left(\pdv{v}{x} \pdv{x}{S}  \right) - rv
$$
che, svolgendo gli opportuni calcoli porta a 
$$
v_{\tau} = v_{xx} + \left(\frac{2r}{\sigma^2}-1\right)v_x - \frac{2r}{\sigma^2}v
$$

Sostituendo rispettivamente i coefficienti di $v_x$ e $v$ con le nuove variabili
$a$ e $b$ possiamo semplificare l'espressione ottenendo una equazione alle
derivate parziali che ci è familiare.

## Formula di BS per European Call Options

Per apprendere la bontà della soluzione numerica ottenuta con il metodo delle
differenze finite calcoliamo la *Formula di Black-Scholes* analiticamente per
una *European Call Option*. Il risultato si ottiene per isomorfismi con
la cosiddetta *Equazione del calore*, e porta alla formula
$$
\begin{split}
V(S, t; r, \sigma, E) = S \, \Phi \left( \frac{- \log(E) + \log(S) + \left(r +
\frac{1}{2} \sigma^2 t\right)}{\sigma\sqrt{t}} \right) +\\- E e^{-rt}  \Phi \left(
\frac{- \log(E) + \log(S) + \left(r - \frac{1}{2} \sigma^2 t
\right)}{\sigma\sqrt{t}} \right)
\end{split}
$$ {#eq:bsf}
dove $\Phi$ è la funzione cumulativa della distribuzione normale definita come
$$
\Phi(x; \sigma, \mu) = \frac{1}{2}\left[1 +
\erf\left(\frac{x-\mu}{\sigma\sqrt{2}}\right)\right]
$$
dove $\mu$ è il *valore di aspettazione*, $\sigma^2$ è la varianza della
distribuzione e $\erf$ è la *error function*.

## Metodo delle differenze finite

Utilizziamo metodi delle differenze finite per convertire il calcolo delle
nostre PDE in un sistema di equazioni che possiamo risolvere con strumenti
algebraici.

Stabiliamo la discretizzazione: dividiamo l'intervallo $\tau \in [0, T]$ in $M$
intervalli di larghezza $\Delta \tau$. Il prezzo del sottostante $S$ corre
sull'intervallo $[0, \infty)$, tuttavia ai fini della computazione dobbiamo
introdurre un limite "artificiale" $S_{\text{max}}$ che fissiamo a $3E$. Quindi
possiamo suddividere l'intervallo $[0, S_{\text{max}}]$ in N intervalli.

Abbiamo quindi come risultato una griglia
$$
(n \Delta S, m \Delta \tau) \in [0, N\Delta S] \times [0, M\Delta \tau],
$$
dove gli $n$ corrono tra 0 e $N$ e gli $m$ corrono tra 0 ed $M$. Con $v_n^m$
indichiamo l'approssimazione numerica di $V(n\Delta S, m \Delta \tau)$. Abbiamo la
condizione al contorno
$$
V(S_{\text{max}}, \tau) = S_{\text{max}} \quad \text{per } t\in[0, T].
$$

![Schema di iterazione](stencil.pdf){width=250px}

Ci avvaliamo del *metodo esplicito* con schema FCTS (*Forward-Time
Central-Space*), approssimando le derivate parziali si $S$ al prim'ordine. I
termini discretizzati assumono dunque la seguente forma
$$
\begin{aligned}
\pdv{V}{t} \/ (n\Delta S, m\Delta t) &= \frac{v_n^{m+1} - v_n^m}{\Delta t} +
O(\Delta t)\\
\pdv{V}{S} \/ (n\Delta S, m\Delta t) &= \frac{v_{n+1}^m - v_{n-1}^m}{2\Delta S}
+ O((\Delta S)^2)\\
\pdv[2]{V}{S} \/ (n\Delta S, m\Delta t) &= \frac{v_{n+1}^m -2v_n^m +
v_{n-1}^m}{(\Delta S)^2} + O((\Delta S)^2)
\end{aligned}
$$

Quindi la forma discretizzata dell'Equazione @eq:bs è
$$
\begin{split}
\frac{v_n^{m+1} - v_n^m}{\Delta t} - \frac{1}{2} \sigma^2 n^2 (\Delta S)^2
\frac{v_{n+1}^m - 2 v_n^m + v_{n-1}^m}{(\Delta S)^2} + \\ - rn\Delta S
\frac{v_{n+1}^m - v_{n-1}^m}{2\Delta S} + rv_n^m = 0,
\end{split}
$$
con $n$ che corre tra 1 e $N-1$ e $m$ che corre tra 0 e $M-1$. L'errore
dell'equazione è dell'ordine di $O(\Delta t + (\Delta S)^2)$. L'equazione
iterativa è dunque
$$
v_n^{m+1} \frac{1}{2} (\alpha n^2 - \beta n) v_{n-1}^m + (1 - \alpha n^2 -
\beta) v_n^m + \frac{1}{2} (\alpha n^2 + \beta n) v_{n+1}^m
$$
per le stesse variabili discrete $n$ ed $m$, dove $\alpha=\sigma^2 \Delta t$ e
$\beta = r\Delta t$.

## Esiti del programma

Dall'esecuzione del programma otteniamo una matrice $V$, che andiamo a plottare.
Abbiamo impostato i parametri del programma con data di scadenza dell'opzione
$T=0.25$, *strike price* $E=10.0$, tasso di interesse $r=0.1$ e volatilità
$\sigma=0.4$. I risultati sono rappresentati nelle seguenti figure.

![European Call Option Price\label{fig:eco1}](fig1.pdf)

![Variance Heatmap\label{fig:std1}](fig2.pdf){width=70%}

Nella prima figura abbiamo scelto di rappresentare il valore dato del prezzo
dell'opzione $V(S, t)$ per ogni punto dello spazio $(S, t)$. Le linee
rappresentano i valori numerici computati, mentre la superficie rappresenta i
valori trovati analiticamente dall'uso dell'Equazione @eq:bsf. La superficie
dello spazio $(S, t)$ è invece caratterizzata da una *heatmap* che rappresenta
la varianza tra i valori numerici e quelli dati dalla *Formula di
Black-Scholes*.

Nella seconda figura riportiamo la medesima *heatmap* delle varianze, calcolate
come valori in quadratura della differenza tra le matrici che rappresentano i
valori numerici e i valori teorici.

# Listato del programma

Il programma compilato da `bs.cpp`, dati i parametri $r, E, T, \sigma$ e
impostate le discretizzazioni date da $N$ e $M$ itera sulle $n$ righe e sulle
$m$ colonne, generando una matrice $(N-1)\times(M-1)$ che viene salvata in un
file con valori separati da spazi.

Il tempo di esecuzione del programma `bs` nella configurazione $(N, M) = (200,
2000)$ è stato di \SI{0.23}{\sec}, mentre nel caso della configurazione $(N, M)
= (2000, 41000)$ è stato di \SI{23.56}{\sec}.

```cpp
#include <iostream>
#include <cmath>
#include <fstream>

// define constants and parameters
const int N = 200;
const int M = 2000;
double T = 0.25;
double E = 10.0;
double r = 0.1;
double sigma = 0.4;
double a = 2*r/pow(sigma, 2) - 1;
double b = -2*r/pow(sigma, 2);
double smax = 30.0;
double delt = T/M;
double dels = smax/(N-1);
double alpha = pow(sigma, 2.0)*delt;
double beta = r*delt;

// define arrays
double S[N];
double t[M];
double v[M][N];
double file[N];

// define indexes
int n = 0, m = 0, i = 0;

// open output file stream
std::ofstream ofile;


int main() {
	ofile.open ("V.dat");

	while(i<N) {
        // initialize initial and border conditions
		S[i] = i*dels;
		if (S[i]-E <= 0){
		    v[i][0] = 0;
		} else {
		    v[i][0] = S[i] - E;
		}
		i += 1;
	}

	for(m = 0; m < M; m++) {
        // find entries and output to file
		t[m] = m*delt;
		v[0][m] = 0;
		v[N-1][m] = smax -E;
		for(n = 1; n < N; n++) {
			v[n][m+1] = 0.5*(alpha*pow(n,2)-beta*n)*v[n-1][m] + (1-alpha*pow(n,2)- beta)*v[n][m] + 0.5*(alpha*pow(n, 2) + beta*n)*v[n+1][m];
            if (n==N-1) {
                ofile << v[n][m];
            } else {
			    ofile<< v[n][m]<< " ";
            }
		}
		ofile << std::endl;
	}
	ofile.close();
	return 0;
}
```
In seguito il file viene letto da un altro programma (nel nostro caso lo script
Python `PlotBS.py`) che produce in uscita un file `pdf` due figure.

```python
# import packages
from mpl_toolkits.mplot3d import axes3d
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

# define Black-Scholes formula
def AnalyticBS(S, t, r, sigma, K):
    return S*norm.cdf((-np.log(K) + np.log(S) + (r + .5*np.power(sigma, 2))*t)/(sigma*np.power(t, .5))) - K*np.exp(-r*t)*norm.cdf((-np.log(K) + np.log(S) + (r - .5*np.power(sigma, 2))*t)/(sigma*np.power(t, .5)))

# load V matrix
V = np.loadtxt('V.dat', dtype=None)

# find matrix of results from BS Formula
Vt = [[AnalyticBS(j/199.0*30.0, i/2000.0*0.25, 0.1, 0.4, 10.0) for i in range(2000)] for j in range(199)]
Vta = np.transpose(np.array([np.array(row) for row in Vt]))

# find matrix of variances and plot heatmap
Vsd = np.power(V1-Vta, 2)
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.set_xlabel('time $t$')
ax2.set_ylabel('stock price $S$')
errcm = ax2.imshow(Vsd, aspect=.007, cmap='hot', interpolation='nearest', extent=[0, 0.25, 0, 30.0])
plt.colorbar(errcm, label='variance')
plt.show()

# plot 3d surface plot
t1 = np.linspace(0,0.25,2000)
s1 = np.linspace(0,30.0,199)
(S1, T1) = np.meshgrid(s1, t1)
fig1 = plt.figure(figsize=(15, 7))
ax1 = fig1.gca(projection='3d')
ax1.set_xlabel('stock price $S$')
ax1.set_ylabel('time $t$')
ax1.set_zlabel('option price $V(S,t)$')
surf1 = ax1.plot_wireframe(S1, T1, V, rstride=100, cstride=10, label='Numerical')
surf2 = ax1.plot_surface(S1, T1, Vta, rstride=20, cstride=5, cmap='viridis', label='Analytical')
errcm1 = ax1.contourf(S1, T1, Vsd, 100, zdir='z', cmap='hot', offset=.3)
ax1.view_init(elev=30, azim=130)
surf2._facecolors2d=surf._facecolors3d
surf2._edgecolors2d=surf._edgecolors3d
plt.colorbar(errcm1, label='variance')
plt.colorbar(surf2, label='option price')
plt.show()

# save figures in pdf files
fig1.savefig('fig1.pdf')
fig2.savefig('fig2.pdf')
```

Nel calcolo eseguito con l'impostazione $N=2000$ e $M=41000$ il programma in
Python ha portato a degli errori che probabilmente possiamo ricondurre alla
dimensione del file. Abbiamo dunque scelto di plottare la figura con l'uso di
*gnuplot*.

# Riferimenti Bibliografici
