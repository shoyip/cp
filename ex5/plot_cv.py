import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv('cvdata.dat', names=['rho', 'cv04', 'cv06', 'cv08', 'cv09', 'cv10', 'cv11', 'cv12', 'cv13', 'cv14', 'cv15'])

fig, ax = plt.subplots()
#plt.plot(df['rho'], df['cv04'], '.--', linewidth=1, label='$T^*=0.4$')
#plt.plot(df['rho'], df['cv06'], '.--', linewidth=1, label='$T^*=0.6$')
#plt.plot(df['rho'], df['cv08'], '.--', linewidth=1, label='$T^*=0.8$')
plt.plot(df['rho'], df['cv09'], '.--', linewidth=1, label='$T^*=0.9$')
plt.plot(df['rho'], df['cv10'], '.--', linewidth=1, label='$T^*=1.0$')
plt.plot(df['rho'], df['cv11'], '.--', linewidth=1, label='$T^*=1.1$')
plt.plot(df['rho'], df['cv12'], '.--', linewidth=1, label='$T^*=1.2$')
plt.plot(df['rho'], df['cv13'], '.--', linewidth=1, label='$T^*=1.3$')
plt.plot(df['rho'], df['cv14'], '.--', linewidth=1, label='$T^*=1.4$')
plt.plot(df['rho'], df['cv15'], '.--', linewidth=1, label='$T^*=1.5$')
plt.grid()
plt.legend(loc='best')
plt.xlabel('rho')
plt.ylabel('$C_v^{res} / Nk$')
plt.savefig('cvplot.pdf')
