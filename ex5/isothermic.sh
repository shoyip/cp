#!/bin/bash
mkdir preproc
touch cvdata.dat
rt=( 0.4 0.6 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 )
rho=( 0.1 0.2 0.3 0.4 0.5 0.6 0.65 0.7 0.75 0.8 0.85 0.9 )
printf "%s\n" "${rho[@]}" > preproc/a.dat
n=0
for j in "${rt[@]}"
do
    touch preproc/isot$n.csv
    for i in "${rho[@]}"
    do
        ./nvt_1 $j $i >>preproc/isot$n.csv
    done
    let "n+=1"
done
paste -d"," preproc/* > cvdata.dat
rm -Rf preproc
python plot_cv.py
