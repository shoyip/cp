#include <iostream>
#include <cmath>
#include <fstream>
#include <random>

using namespace std;
#define LOG(x) cout << x << endl

const int N = 125;
const int n = 5;
const double kb = 8.6173e-5; // eV K^-1
const double sigma = 3.345; //Angstrom
const double eps = 125.7*kb;
double T = (300)*kb/eps;
double beta = 1/T;
double L;

void concon(double x);
double lj(double r);
double ljf(double r);

int main(){
    double V, V1, A;
    double ranx, rany, ranz;
    double x[N], x1[N], y[N],y1[N], z[N], z1[N];
    int div = 15;
    double rho[div];


    double press[div];
    double vol[div];
    double Cv[div];
    int steps = 80000;
    double rhomin = 0.001;
    double rhomax = 1.2;
    for(int i = 0; i<div; i++){
        rho[i] = rhomin + i*((rhomax-rhomin)/(div-1));
    }
    
    ofstream file;
    ofstream calvol;
    ofstream pot;
    pot.open("potenziale_cv.dat");
    calvol.open("CV_cv.dat");
    file.open("isoterma_cv.dat");

    //ciclo densità
    for(int q=0; q <div; q++){
        LOG("--------------");
        //grandezze dipendenti da rho
        L = pow(N/rho[q], 1.0/3.0);
        LOG(L);
        LOG(rho[q]);
        double de_x = L/5;
        double delta[div];
        delta[q] = L/(q/2+0.5);
        double meanV = 0; 
        double meanV2 = 0;
        double meanf = 0;
        double force = 0;
        double contatore = 0;
        V = 0;
        //griglia posizioni
        int c = 0;
        for(int i=0;i<n; i++){
		    for(int j=0;j<n; j++){
			    for(int k=0;k<n; k++){    
			        x[c] = i*de_x + 0.5*de_x;
			        y[c] = j*de_x + 0.5*de_x;
			        z[c] = k*de_x + 0.5*de_x;
                    c++;
			    }
		    }
	    }
        //potenziale iniziale
        for(int i=0; i<N; i++){
            for(int j=0; j<i; j++){
                double dx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                double dy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                double dz = z[i] - z[j] - L*rint((z[i] - z[j])/L);                    
                double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                V += lj(r);
            }
        }
        //ciclo steps
        for(int t =0; t<steps; t++){
            //grandezze da annullare ogni ciclo
            V1 = 0;
            force = 0;

            //per ogni ciclo estraggo una particella a caso e propongo uno spostamento
            int prop = rand() % 125;
           
            ranx = (double)rand()/ RAND_MAX;
            rany = (double)rand()/ RAND_MAX;
            ranz = (double)rand()/ RAND_MAX;

            x1[prop] = x[prop] + delta[q]*(ranx - 0.5);
            y1[prop] = y[prop] + delta[q]*(rany - 0.5);
            z1[prop] = z[prop] + delta[q]*(ranz - 0.5);
            concon(x[prop]);
            concon(y[prop]);
            concon(z[prop]);

            for(int i= 0; i<N; i++){
                if(i!=prop){
                    x1[i] = x[i];
                    y1[i] = y[i];
                    z1[i] = z[i];
                }
            }
            //calcolo il potenziale della nuova configurazione
            for(int i=0; i<N; i++){
                for(int j=0; j<i; j++){
                    double dx = x1[i] - x1[j] - L*rint((x1[i] - x1[j])/L);
                    double dy = y1[i] - y1[j] - L*rint((y1[i] - y1[j])/L);
                    double dz = z1[i] - z1[j] - L*rint((z1[i] - z1[j])/L);
                    double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                    V1 += lj(r);
                }
            }

            //se è minore di quello precedente lo accetto
            if(V1<V){
                V = V1;
                x[prop] = x1[prop];
                y[prop] = y1[prop];
                z[prop] = z1[prop];
                contatore += 1;                         

            }else{
                A = exp(-beta*(V1 - V));
                double eta = (double)rand()/RAND_MAX;
                if(eta < A ){
                    V = V1;
                    x[prop] = x1[prop];
                    y[prop] = y1[prop];
                    z[prop] = z1[prop];   
                    contatore += 1;             
                }                                    
            }

            for(int i = 0; i<N; i++){
                for(int j=0; j<N ; j++){
                    if(i != j){
                        double dx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                        double dy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                        double dz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                        double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                        force += ljf(r)*r;
                    }
                }
            }
            
            if(t>14999){
                meanV += V;
                meanV2 += pow(V, 2);
                meanf += force;
            
            }
            if(q==1){
                pot<<t+1<<" "<<V<<endl;
            }
            delta[q] = 0.99995*delta[q];

        }//fine ciclo steps
        //valori medi delle grandezze
        int ste = steps - 15000 ;
        meanV = meanV/ste;
        meanV2 = meanV2/ste;
        Cv[q] = 3*N/2 + (meanV2 - pow(meanV, 2))/(pow(T, 2)/kb);
        meanf = 0.5*force/ste;
        press[q] = N*T/vol[q] - meanf/(3*vol[q]);
        file<<press[q]<<"  "<<vol[q]<<endl;
        calvol<<rho[q]<<"   "<<Cv[q]<<endl;

        LOG(V);
        LOG(contatore);

    }//fine ciclo densità
    pot.close();
    file.close();
    calvol.close();
    return 0;
}

//condizioni al contorno 
void concon(double x){
    if(x<0){
        x += L;
    }
    if(x>L){
        x -= L;
    }
}
//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*(pow(1/r,12)-pow(1/r, 6));
	}
	
}

//funzione per la forza
double ljf(double r){
    if (r == 0){
        return 0;
    }else{
        return 24*(pow(r, -7)-2*pow(r,-13));
    }
}