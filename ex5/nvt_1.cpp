#include <iostream>
#include <cmath>
#include <fstream>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <string>

using namespace std;

const int N = 125;
const int n = 5;
const double kb = 0.001987; // kcal/K*mol
const double sigma = 3.405; // angstrom
const double epsilon = 0.238; // kcal/mol
const double mass = 39.95; // g/mol
double rt;
double rho;
double L;

int mcsteps = 1000000; // total steps
int thsteps = 100000; // thermalization steps
int retrieval = 5000; // data retrieval interval

struct atom { double x, y, z; };

void concon (double x);
double LJPairErg (struct atom atoms[N], int a, int b);
double LJPairErg0 (struct atom atoms[N], int a, double xo, double yo, double zo);
double totPairErg (struct atom atoms[N]);
double deltaE (struct atom atoms[N], int idx, double xo, double yo, double zo);
double probability (double dE, double Beta);

int main (int argc, char* argv[]) {
    rt = stod(argv[1]);
    rho = stod(argv[2]);
    double temp = rt*epsilon/kb;
    double Beta = 1./(kb*temp);
    double dist = pow(1./rho, 1./3.);
    L = dist*n;

    srand(time(NULL));

    double xold, yold, zold, xnew, ynew, znew, Ep, dE, eta, prob;
    double erg_sum, ergsq_sum, erg_xp, ergsq_xp, cv;
    double delta = 1.*pow(10, -1.64*rho-1.64);
    int rand_a;
    int statcount = 0;
    int acccount = 0;
    int refcount = 0;

    // initialize grid
    struct atom atoms[N];
    for (int i=0; i<N; i++) {
        atoms[i].x = (.5+(double)(i/(n*n)))*dist;
        atoms[i].y = (.5+(double)(i%(n*n)/n))*dist;
        atoms[i].z = (.5+(double)(i%n))*dist;
    }

    // energy of initial config
    double energy = totPairErg(atoms);
    //cout << "Initial energy is " << energy << "." << endl;
    //cout << "Starting sampling loop..." << endl;
    for (int t=0; t<mcsteps; t++) {
        Ep = energy;
        rand_a = rand() % N;

        xold = atoms[rand_a].x;
        yold = atoms[rand_a].y;
        zold = atoms[rand_a].z;

        xnew = xold + delta*((double)rand() / RAND_MAX - .5);
        ynew = yold + delta*((double)rand() / RAND_MAX - .5);
        znew = zold + delta*((double)rand() / RAND_MAX - .5);

        concon(xnew);
        concon(ynew);
        concon(znew);
        
        atoms[rand_a].x = xnew;
        atoms[rand_a].y = ynew;
        atoms[rand_a].z = znew;

        dE = deltaE(atoms, rand_a, xold, yold, zold);
        prob = probability(dE, Beta);
        eta = (double)rand() / RAND_MAX;
        // cout << "prob = " << prob << ", eta = " << eta << endl;

        if (dE>=0 && eta>prob) {
            atoms[rand_a].x = xold;
            atoms[rand_a].y = yold;
            atoms[rand_a].z = zold;
            // cout << "REFUSED" << endl;
            refcount++;
        } else {
            acccount++;
        }

        energy += dE;

        if ((double)acccount/t < 0.3) {
            delta *= 1.000000005;
        } else if ((double)acccount > 0.7) {
            delta *= 0.999999995;
        }
        
        if (t>thsteps && t%retrieval==0) {
            statcount++;
            erg_sum += energy;
            ergsq_sum += energy*energy;
            erg_xp = erg_sum / (double)statcount;
            ergsq_xp = ergsq_sum / (double)statcount;
            cv = (ergsq_xp - erg_xp*erg_xp)*(Beta/temp);
            // cout << "ergsq_xp = " << ergsq_xp << "erg_xp^2 = " << erg_xp*erg_xp << "cv = " << cv << endl;
        }

//        if (t>thsteps && t%100000==0) cout << "cv at step " << t << " is " << cv << "." << endl;
    }
//    cout << "Final result for Cv = " << cv/(N*kb) << "." << endl;
//    cout << "Acceptance rate is " << (double)acccount/mcsteps << "." << endl;
      cout << cv/(N*kb) << endl;
}

void concon (double x) {
    if (x<0) x+=L;
    if (x>L) x-=L;
}

double LJPairErg (struct atom atoms[N], int a, int b) {
    double rcs = (2.7*sigma)*(2.7*sigma);
    double rs = pow(atoms[a].x - atoms[b].x, 2.) + pow(atoms[a].y - atoms[b].y, 2.) + pow(atoms[a].z - atoms[b].z, 2.);
    double result = rs<rcs ? 4.*epsilon*(pow(sigma/rs, 6.) - pow(sigma/rs, 3.)) : 0.;
    return result;
}

double LJPairErg0 (struct atom atoms[N], int a, double xo, double yo, double zo) {
    double rcs = (2.7*sigma)*(2.7*sigma);
    double rs = pow(atoms[a].x - xo, 2.) + pow(atoms[a].y - yo, 2.) + pow(atoms[a].z - zo, 2.);
    double result = rs<rcs ? 4.*epsilon*(pow(sigma/rs, 6.) - pow(sigma/rs, 3.)) : 0.;
    return result;
}

double totPairErg (struct atom atoms[N]) {
    double Et = 0.;
    for (int i=0; i<N-1; i++) {
        for (int j=i+1; j<N; j++) {
            Et += LJPairErg(atoms, i, j);
        }
    }
    return Et;
}

double deltaE (struct atom atoms[N], int idx, double xo, double yo, double zo) {
    double Et = 0.;
    double Eto = 0.;
    for (int i=0; i<N; i++) {
        if (i != idx) {
            Et += LJPairErg(atoms, idx, i);
            Eto += LJPairErg0(atoms, i, xo, yo, zo);
        }
    }
    double dE = Et - Eto;
    return dE;
}

double probability (double dE, double Beta) {
    return exp(-Beta*dE);
}
