import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt("potenziale_cv.dat")

t = data[:,0]
E = data[:,1]

plt.plot(t, E, 'b')
plt.show()