import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt('isoterma.dat')

P = data[1:,0]
V = data[1:,1]

vo = np.linspace(950,12500, 10000)
pr = (125*120/125.7)/vo


i1, = plt.plot(V, P, 'g.')
i2, = plt.plot(vo, pr, color='lightcoral', linewidth=1)
plt.xlabel("Volume")
plt.ylabel("Pressione")
plt.legend([i1,i2], ['Dati', 'Modello'])
plt.grid()
plt.show()


data2 = np.loadtxt('CV_cv.dat')

rho = data2[:12,0]
cv = data2[:12,1]/125
plt.plot(rho, cv, '.r')
plt.xlabel("Densità")
plt.ylabel("Cv")
plt.grid()
plt.show()