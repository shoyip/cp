// Include libraries
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <random>
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;

// Exercise 5: MC SIMULATION OF ARGON PARTICLE BOX The goal of this exercise is
// to find the isothermal PV curves, the corresponding Cv values vs the density
// and an estimate of the critical temperatures.

// Definitions
#define LOG(x) cout << x << endl

// Define the parameters of the problem
// Using LAMMPS lj style units

// Fundamental quantities
const double kb = 1.;
double sig = 3.405; // angstrom
double eps = 0.010831984; // eV
//double eps = 0.24979149; // Kcal/mol
double mas = 39.948; // au

// Problem parameters
double rc = 2.7*sig;
double d = .85;
const int dim = 3;
const int N = 125; // always set powers of dim
int tmax;
int idx;

// System properties
//double rT0 = (273.15 + 87) * kb / eps;
//double rT0 = 120 * kb / eps;
double rT0 = 1.;
double rrho = 0.08;
double rL = pow(N/rrho, 1./3.);
double gap = pow(1/rrho, 1./3.);

// Define subroutines

double ran (double var) {
    double u, v, x;
    double pi = 3.1415926535;
    u = (double)rand() / RAND_MAX;
    v = (double)rand() / RAND_MAX;
    x = sqrt(-2*pow(var, 2)*log(1-u)) * cos(2*pi*v);
    return x;
}

void initialize (double x[N][dim], double v[N][dim], double f[N][dim], double L, double T) {
    // Initialize positions, velocities and forces grid
    for (int i=0; i<N; i++) {
        x[i][0] = (.5+(double)(i/25))*gap;
        x[i][1] = (.5+(double)(i%25/5))*gap;
        x[i][2] = (.5+(double)(i%5))*gap;
        for (int j=0; j<dim; j++) {
            v[i][j] = ran(sqrt(T));
            f[i][j] = 0.;
        }
        //cout << "Particle #" << i << " is in position (" << x[i][0] << ", " << x[i][0] << ", " << x[i][0] <<  ")" << endl;
    }
}

void bc (double x[N][dim], double L) {
    for (int i=0; i<N; i++) {
        for (int j=0; j<dim; j++) {
            if (x[i][j]<0) x[i][j]+=L;
            if (x[i][j]>L) x[i][j]-=L;
        }
    }
}

double potential (double x[N][dim], double rc, double L) {
    double dx, r2, pot_t=0.;
    double r2c = rc*rc;
    pot_t = 0.;
    // Loop over all pairs
    for (int i=0; i<N; i++) {
        for (int j=i+1; j<N; j++) {
            dx = 0.;
            r2 = 0.;
            // For every ax, find distances applying pbcs
            for (int k=0; k<dim; k++) {
                dx = x[i][k] - x[j][k] - L*rint((x[i][k] - x[j][k])/L);
                r2 += dx*dx;
            }
            if (r2 < r2c) {
                pot_t += 4.*( 1./pow(r2, 6.) - 1./pow(r2, 3.) );
            }
        }
    }
    //cout << "V = " << pot_t << endl;
    return pot_t;
}

// Define main function

int main(int argc, char *argv[]) {
    srand(time(NULL));
    tmax = std::atoi(argv[1]);
    LOG(tmax);
    // Define the variables of the problem
    double x[N][dim];
    double v[N][dim];
    double f[N][dim];

    // Define the main observables
    double V[tmax];

    // Initialize positions, velocities and forces
    initialize(x, v, f, rL, rT0);
    double V0;
    V0=potential(x, rc, rL);

    cout << "OK NOW potential is " << V0 << endl;
    int t=0;
    int trials=0;
    ofstream file;
    file.open("energies.dat");
    double beta = 1/(kb*rT0);
    while (t<tmax) {
        // Attempts moving particles randomly one at a time
        double oldx;
        idx = rand() % 125; // picks randomly an index between 0 and 125
        cout << "Actual Delta value is " << d << endl;
        for (int i=0; i<dim; i++) {
            oldx = x[idx][i];
            x[idx][i] += d*(rand()/RAND_MAX - .5);
            cout << "Proposed displacement for axis " << i << ": from " << oldx << " to " << x[idx][i] << " for particle N. " << idx << endl;
        }

        // Apply border conditions
        bc(x, rL);

        // Find potential energy of config
        V[t] = potential(x, rc, rL);
        cout << "Potential is " << V[t] << endl;

        // Find the difference in energy
        double E0, E, delE, ranf, exp_p;
        t==0 ? E0=V0 : E0=V[t-1];
        E = V[t];
        delE = E-E0;

        // Acceptance test
        if (delE <= 0) {
            t+=1;
            file << E0 << endl;
            cout << "== ACCEPTED ==" << endl;
        } else {
            ranf = (double)rand()/RAND_MAX;
            exp_p = exp(-delE/(kb*rT0));
            if (ranf <= exp_p) {
                file << E0 << endl;
                cout << "== ACCEPTED with probability " << std::setprecision(7) << exp_p << " ==" << endl;
                t+=1;
            } else {
                cout << "== REFUSED ==" << endl;
            }
        }
        cout << "Energy is " << E << endl;
        cout << "Energy displacement is " << delE << endl;
        cout << "Exp probability is " << exp_p << endl;
        cout << "Random float is " << ranf << endl;
        cout << endl;
        trials += 1;

        if (trials % 20 == 0) {
            if (t/trials < .3) {
                d *= 1.2;
            } else if (t/trials > .7) {
                d *= .8;
            }
        }
    }
    file.close();

    cout << trials << " trials were performed. " << t << " were successful." << endl;
    cout << "rT0 = " << rT0 << endl;
    cout << "beta = " << beta << endl;
}
