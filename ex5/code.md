Codice utilizzato per calcolare la curva nel piano $PV$.

```cpp
#include <iostream>
#include <cmath>
#include <fstream>
#include <random>

using namespace std;
#define LOG(x) cout << x << endl

const int N = 125;
const int n = 5;
const double kb = 8.6173e-5; // eV K^-1
const double sigma = 3.345; //Angstrom
const double eps = 125.7*kb;
double T = (120)*kb/eps;
double beta = 1/T;
double L;

void concon(double x);
double lj(double r);
double ljf(double r);

int main(){
    double V, V1, A;
    double ranx, rany, ranz;
    double x[N], x1[N], y[N],y1[N], z[N], z1[N];
    int div = 15;
    double rho[div];


    double press[div];
    double vol[div];
    double Cv[div];
    int steps = 100000;
    double volmin = 100;
    double volmax = 12500;
    for(int i = 0; i<div; i++){
        vol[i] = volmin + i*((volmax-volmin)/(div-1));
    }
    
    ofstream file;
    ofstream calvol;
    ofstream pot;
    pot.open("potenziale.dat");
    calvol.open("CV.dat");
    file.open("isoterma.dat");

    //ciclo densità
    for(int q=0; q <div; q++){
        LOG("--------------");
        //grandezze dipendenti da rho
        L = pow(vol[q], 1.0/3.0);
        LOG(L);
        rho[q] = N/vol[q];
        LOG(rho[q]);
        double de_x = L/5;
        double delta[div];
        delta[q] = (q/2 +0.5)*L;
        double meanV = 0; 
        double meanV2 = 0;
        double meanf = 0;
        double force = 0;
        double contatore = 0;
        V = 0;
        //griglia posizioni
        int c = 0;
        for(int i=0;i<n; i++){
		    for(int j=0;j<n; j++){
			    for(int k=0;k<n; k++){    
			        x[c] = i*de_x + 0.5*de_x;
			        y[c] = j*de_x + 0.5*de_x;
			        z[c] = k*de_x + 0.5*de_x;
                    c++;
			    }
		    }
	    }
        //potenziale iniziale
        for(int i=0; i<N; i++){
            for(int j=0; j<i; j++){
                double dx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                double dy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                double dz = z[i] - z[j] - L*rint((z[i] - z[j])/L);                    
                double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                V += lj(r);
            }
        }
        //ciclo steps
        for(int t =0; t<steps; t++){
            //grandezze da annullare ogni ciclo
            V1 = 0;
            force = 0;

            //per ogni ciclo estraggo una particella a caso e propongo uno spostamento
            int prop = rand() % 125;
           
            ranx = (double)rand()/ RAND_MAX;
            rany = (double)rand()/ RAND_MAX;
            ranz = (double)rand()/ RAND_MAX;

            x1[prop] = x[prop] + delta[q]*(ranx - 0.5);
            y1[prop] = y[prop] + delta[q]*(rany - 0.5);
            z1[prop] = z[prop] + delta[q]*(ranz - 0.5);
            concon(x[prop]);
            concon(y[prop]);
            concon(z[prop]);

            for(int i= 0; i<N; i++){
                if(i!=prop){
                    x1[i] = x[i];
                    y1[i] = y[i];
                    z1[i] = z[i];
                }
            }
            //calcolo il potenziale della nuova configurazione
            for(int i=0; i<N; i++){
                for(int j=0; j<i; j++){
                    double dx = x1[i] - x1[j] - L*rint((x1[i] - x1[j])/L);
                    double dy = y1[i] - y1[j] - L*rint((y1[i] - y1[j])/L);
                    double dz = z1[i] - z1[j] - L*rint((z1[i] - z1[j])/L);
                    double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                    V1 += lj(r);
                }
            }

            //se è minore di quello precedente lo accetto
            if(V1<V){
                V = V1;
                x[prop] = x1[prop];
                y[prop] = y1[prop];
                z[prop] = z1[prop];
                contatore += 1;                         

            }else{
                A = exp(-beta*(V1 - V));
                double eta = (double)rand()/RAND_MAX;
                if(eta < A ){
                    V = V1;
                    x[prop] = x1[prop];
                    y[prop] = y1[prop];
                    z[prop] = z1[prop];   
                    contatore += 1;             
                }                                    
            }

            for(int i = 0; i<N; i++){
                for(int j=0; j<N ; j++){
                    if(i != j){
                        double dx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                        double dy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                        double dz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                        double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                        force += ljf(r)*r;
                    }
                }
            }
            
            if(t>19999){
                meanV += V;
                meanV2 += pow(V, 2);
                meanf += force;
            
            }
            if(q==1){
                pot<<t+1<<" "<<V<<endl;
            }
            delta[q] = 0.99995*delta[q];

        }//fine ciclo steps
        //valori medi delle grandezze
        int ste = steps - 20000 ;
        meanV = meanV/ste;
        meanV2 = meanV2/ste;
        Cv[q] = 3*N/2 + (meanV2 - pow(meanV, 2))/pow(T, 2);
        meanf = 0.5*force/ste;
        press[q] = N*T/vol[q] - meanf/(3*vol[q]);
        file<<press[q]<<"  "<<vol[q]<<endl;
        calvol<<rho[q]<<"   "<<Cv[q]<<endl;

        LOG(V);
        LOG(contatore);

    }//fine ciclo densità
    pot.close();
    file.close();
    calvol.close();
    return 0;
}

//condizioni al contorno 
void concon(double x){
    if(x<0){
        x += L;
    }
    if(x>L){
        x -= L;
    }
}
//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*(pow(1/r,12)-pow(1/r, 6));
	}
	
}

//funzione per la forza
double ljf(double r){
    if (r == 0){
        return 0;
    }else{
        return 24*(pow(r, -7)-2*pow(r,-13));
    }
}
```

Codice Python utilizzato per ricavare la figura della curva isoterma sul piano $PV$.

```python
import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt('isoterma.dat')

P = data[1:,0]
V = data[1:,1]

vo = np.linspace(950,12500, 10000)
pr = (125*120/125.7)/vo


i1, = plt.plot(V, P, 'g.')
i2, = plt.plot(vo, pr, color='lightcoral', linewidth=1)
plt.xlabel("Volume")
plt.ylabel("Pressione")
plt.legend([i1,i2], ['Dati', 'Modello'])
plt.grid()
plt.show()
```

Codice C++ utilizzato per calcolare i valori di $C_v^{\text{res}}$ in funzione della densità per varie temperature.

```cpp
#include <iostream>
#include <cmath>
#include <fstream>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <string>

using namespace std;

const int N = 125;
const int n = 5;
const double kb = 0.001987; // kcal/K*mol
const double sigma = 3.405; // angstrom
const double epsilon = 0.238; // kcal/mol
const double mass = 39.95; // g/mol
double rt;
double rho;
double L;

int mcsteps = 1000000; // total steps
int thsteps = 100000; // thermalization steps
int retrieval = 5000; // data retrieval interval

struct atom { double x, y, z; };

void concon (double x);
double LJPairErg (struct atom atoms[N], int a, int b);
double LJPairErg0 (struct atom atoms[N], int a, double xo, double yo, double zo);
double totPairErg (struct atom atoms[N]);
double deltaE (struct atom atoms[N], int idx, double xo, double yo, double zo);
double probability (double dE, double Beta);

int main (int argc, char* argv[]) {
    rt = stod(argv[1]);
    rho = stod(argv[2]);
    double temp = rt*epsilon/kb;
    double Beta = 1./(kb*temp);
    double dist = pow(1./rho, 1./3.);
    L = dist*n;

    srand(time(NULL));

    double xold, yold, zold, xnew, ynew, znew, Ep, dE, eta, prob;
    double erg_sum, ergsq_sum, erg_xp, ergsq_xp, cv;
    double delta = 1.*pow(10, -1.64*rho-1.64);
    int rand_a;
    int statcount = 0;
    int acccount = 0;
    int refcount = 0;

    // initialize grid
    struct atom atoms[N];
    for (int i=0; i<N; i++) {
        atoms[i].x = (.5+(double)(i/(n*n)))*dist;
        atoms[i].y = (.5+(double)(i%(n*n)/n))*dist;
        atoms[i].z = (.5+(double)(i%n))*dist;
    }

    // energy of initial config
    double energy = totPairErg(atoms);
    //cout << "Initial energy is " << energy << "." << endl;
    //cout << "Starting sampling loop..." << endl;
    for (int t=0; t<mcsteps; t++) {
        Ep = energy;
        rand_a = rand() % N;

        xold = atoms[rand_a].x;
        yold = atoms[rand_a].y;
        zold = atoms[rand_a].z;

        xnew = xold + delta*((double)rand() / RAND_MAX - .5);
        ynew = yold + delta*((double)rand() / RAND_MAX - .5);
        znew = zold + delta*((double)rand() / RAND_MAX - .5);

        concon(xnew);
        concon(ynew);
        concon(znew);
        
        atoms[rand_a].x = xnew;
        atoms[rand_a].y = ynew;
        atoms[rand_a].z = znew;

        dE = deltaE(atoms, rand_a, xold, yold, zold);
        prob = probability(dE, Beta);
        eta = (double)rand() / RAND_MAX;

        if (dE>=0 && eta>prob) {
            atoms[rand_a].x = xold;
            atoms[rand_a].y = yold;
            atoms[rand_a].z = zold;
            refcount++;
        } else {
            acccount++;
        }

        energy += dE;

        if ((double)acccount/t < 0.3) {
            delta *= 1.000000005;
        } else if ((double)acccount > 0.7) {
            delta *= 0.999999995;
        }
        
        if (t>thsteps && t%retrieval==0) {
            statcount++;
            erg_sum += energy;
            ergsq_sum += energy*energy;
            erg_xp = erg_sum / (double)statcount;
            ergsq_xp = ergsq_sum / (double)statcount;
            cv = (ergsq_xp - erg_xp*erg_xp)*(Beta/temp);
        }
    }
      cout << cv/(N*kb) << endl;
}

void concon (double x) {
    if (x<0) x+=L;
    if (x>L) x-=L;
}

double LJPairErg (struct atom atoms[N], int a, int b) {
    double rcs = (2.7*sigma)*(2.7*sigma);
    double rs = pow(atoms[a].x - atoms[b].x, 2.) + pow(atoms[a].y - atoms[b].y, 2.) + pow(atoms[a].z - atoms[b].z, 2.);
    double result = rs<rcs ? 4.*epsilon*(pow(sigma/rs, 6.) - pow(sigma/rs, 3.)) : 0.;
    return result;
}

double LJPairErg0 (struct atom atoms[N], int a, double xo, double yo, double zo) {
    double rcs = (2.7*sigma)*(2.7*sigma);
    double rs = pow(atoms[a].x - xo, 2.) + pow(atoms[a].y - yo, 2.) + pow(atoms[a].z - zo, 2.);
    double result = rs<rcs ? 4.*epsilon*(pow(sigma/rs, 6.) - pow(sigma/rs, 3.)) : 0.;
    return result;
}

double totPairErg (struct atom atoms[N]) {
    double Et = 0.;
    for (int i=0; i<N-1; i++) {
        for (int j=i+1; j<N; j++) {
            Et += LJPairErg(atoms, i, j);
        }
    }
    return Et;
}

double deltaE (struct atom atoms[N], int idx, double xo, double yo, double zo) {
    double Et = 0.;
    double Eto = 0.;
    for (int i=0; i<N; i++) {
        if (i != idx) {
            Et += LJPairErg(atoms, idx, i);
            Eto += LJPairErg0(atoms, i, xo, yo, zo);
        }
    }
    double dE = Et - Eto;
    return dE;
}

double probability (double dE, double Beta) {
    return exp(-Beta*dE);
}
```

Codice Python per ricavare le curve di $C_v^{\text{res}}$ in funzione di $\rho$ per diverse temperature.

```python
import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv('cvdata.dat', names=['rho', 'cv04', 'cv06', 'cv08', 'cv09', 'cv10', 'cv11', 'cv12', 'cv13', 'cv14', 'cv15'])

fig, ax = plt.subplots()
plt.plot(df['rho'], df['cv04'], '.--', linewidth=1, label='$T^*=0.4$')
plt.plot(df['rho'], df['cv06'], '.--', linewidth=1, label='$T^*=0.6$')
plt.plot(df['rho'], df['cv08'], '.--', linewidth=1, label='$T^*=0.8$')
plt.plot(df['rho'], df['cv09'], '.--', linewidth=1, label='$T^*=0.9$')
plt.plot(df['rho'], df['cv10'], '.--', linewidth=1, label='$T^*=1.0$')
plt.plot(df['rho'], df['cv11'], '.--', linewidth=1, label='$T^*=1.1$')
plt.plot(df['rho'], df['cv12'], '.--', linewidth=1, label='$T^*=1.2$')
plt.plot(df['rho'], df['cv13'], '.--', linewidth=1, label='$T^*=1.3$')
plt.plot(df['rho'], df['cv14'], '.--', linewidth=1, label='$T^*=1.4$')
plt.plot(df['rho'], df['cv15'], '.--', linewidth=1, label='$T^*=1.5$')
plt.grid()
plt.legend(loc='best')
plt.xlabel('rho')
plt.ylabel('$C_v^{res} / Nk$')
plt.savefig('cvplot.pdf')
```

Script Bash per eseguire il programma `nvt_1` per varie densità per varie temperature e ottenere con il precedente `plot_cv.py` il grafico di $C_v^{\text{res}}$.

```bash
#!/bin/bash
mkdir preproc
touch cvdata.dat
rt=( 0.4 0.6 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 )
rho=( 0.1 0.2 0.3 0.4 0.5 0.6 0.65 0.7 0.75 0.8 0.85 0.9 )
printf "%s\n" "${rho[@]}" > preproc/a.dat
n=0
for j in "${rt[@]}"
do
    touch preproc/isot$n.csv
    for i in "${rho[@]}"
    do
        ./nvt_1 $j $i >>preproc/isot$n.csv
    done
    let "n+=1"
done
paste -d"," preproc/* > cvdata.dat
rm -Rf preproc
python plot_cv.py
```
