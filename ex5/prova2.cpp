#include <iostream>
#include <cmath>
#include <fstream>
#include <random>
#define LOG(x) cout << x << endl

using namespace std;

const int N = 125;
const int n = 5;
const double kb = 8.6173e-5; // eV K^-1
const double sigma = 3.345; //Angstrom
const double eps = 125.7*kb;
double T = (273 + 87)*kb/eps;
double beta = 1/T;
double L;
double x[N];
double y[N];
double z[N];

void concon(double x);
double lj(double r);
double ljf(double r);


int main(){

    double rho[] = {0.001, 0.01, 0.05, 0.1, 1, 1.2};
    double x1[N], y1[N], z1[N];
    double V, V1, A;
    double ranx, rany, ranz;
    double press[6];
    double vol[6];
    int steps = 2000;
    
    ofstream file;
    file.open("isoterma.dat");

    for(int q = 0; q< 6; q++){
        vol[q] = N/rho[q];
        L = pow(vol[q], 1.0/3.0);
        double de_x = L/5;
        double delta = L/1000;
        double meanV = 0; 
        double meanV2 = 0;
        double meanf = 0;
        double force = 0;
        double contatore = 0; 

        int c = 0;
        //griglia posizioni
        for(int i=0;i<n; i++){
		    for(int j=0;j<n; j++){
			    for(int k=0;k<n; k++){    
			        x[c] = i*de_x + 0.5*de_x;
			        y[c] = j*de_x + 0.5*de_x;
			        z[c] = k*de_x + 0.5*de_x;
                    c++;
			    }
		    }
	    }
        LOG("Griglia inizializzata!");
        for(int k=0; k <steps; k++ ){ // ciclo totale
            V = 0;
            V1 = 0;
            for(int j=0; j<N; j++){
                for(int i = 0; i<N; i++){
                    if(i!=j){
                        // Poniamo le condizioni di Box - Von Karman
                        double dx = x[i] - x[j] - L*rint((x[i] - x[j])/L);
                        double dy = y[i] - y[j] - L*rint((y[i] - y[j])/L);
                        double dz = z[i] - z[j] - L*rint((z[i] - z[j])/L);
                        double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                        V += lj(r)/2;
                    }
                }
            }
            // ciclo proposte posizioni
            // Per ciascuna coppia ij off-diagonal proponiamo degli spostamenti
            for(int i = 0; i<N ; i++){
                // estraggo numeri casuali ranx, rany, ranz
                ranx = (double)rand()/ RAND_MAX;
                rany = (double)rand()/ RAND_MAX;
                ranz = (double)rand()/ RAND_MAX;

                x1[i] = x[i] + delta*(ranx - 0.5);
                y1[i] = y[i] + delta*(rany - 0.5);
                z1[i] = z[i] + delta*(ranz - 0.5);

                // Imponiamo le condizioni al contorno (le particelle NON escono dalla box)
                concon(x[i]);
                concon(y[i]);
                concon(z[i]);
            }
            LOG("Proposti i displacement!");

            // ricalcolo l'energia potenziale e accetto la transizione per probabilità
            for(int j=0; j<N; j++){
                for(int i = 0; i<N; i++){
                    if(i!=j){
                        double dx = x1[i] - x1[j] - L*rint((x1[i] - x1[j])/L);
                        double dy = y1[i] - y1[j] - L*rint((y1[i] - y1[j])/L);
                        double dz = z1[i] - z1[j] - L*rint((z1[i] - z1[j])/L);
                        double r = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
                        V1 += lj(r)/2;
                    }
                }
            }
            A = exp(-beta*(V - V1));
            double cc = (double)rand()/RAND_MAX;
            //cout<<cc<<" "<<A<<endl;
            if(cc<A){
                V = V1;
                for(int i=0; i<N; i++){
                    x[i] = x1[i];
                    y[i] = y1[i];
                    z[i] = z1[i];
                }
                contatore += 1;
                meanV += V;
                meanV2 += pow(V, 2);
                for(int i =0; 1<N; i++){
                    for(int j =0; j<N; j++){
                        if(i!=j){
                            double rij = sqrt(pow(x[i]-x[j], 2) + pow(y[i]- y[j], 2) + pow(z[i] - z[j], 2));
                            force += 0.5*ljf(rij)*rij;
                        }
                    }
                }
            }
        
        }chiudo steps
        //valori medi delle grandezze
        meanV = meanV/contatore;
        meanV2 = meanV2/contatore;
        double Cv = 3/2 * N*kb + (meanV2 - pow(meanV, 2))/(kb*pow(T, 2));
        meanf = force/contatore;
        press[q] = n*kb*T/vol[q] - meanf/(3*vol[q]);
        file<<press[q]<<"  "<<vol[q]<<endl;

    }//chiudo q

    return 0;
}

//condizioni al contorno 
void concon(double x){
    if(x<0){
        x += L;
    }
    if(x>L){
        x -= L;
    }
}
//funzione per il potenziale lj.
double lj(double r){
	if (r == 0){
		return 0;
	}else{
		return 4*(pow(1/r,12)-pow(1/r, 6));
	}
	
}

//funzione per la forza
double ljf(double r){
    if (r == 0){
        return 0;
    }else{
        return 24*(pow(r, -7)-2*pow(r,-13));
    }
}
