#!/bin/bash

# This script runs the program `neutstar` (make sure you have it in your current
# folder!) for different values of n, then makes a plot of them and exports it
# in pdf format. Then cleans data directory.

mkdir data data2
touch xibar.dat

array1=('1.5' '1.7' '2.1' '2.5' '3.0')

for i in "${array1[@]}"; do
	echo -e "$i" >> xibar0.dat
	./neutstar $i data/theta$i.dat >> xibar1.dat;
	paste xibar0.dat xibar1.dat > xibar.dat;
done

paste data/xi.dat data/theta*.dat > data/output.dat;
gnuplot plotscript

array2=('1.0' '4.5')

for i in "${array2[@]}"; do
	./neutstar $i data2/theta$i.dat;
done

paste data/xi.dat data2/theta*.dat > data2/output.dat;
gnuplot plotscript2

rm -Rf data data2 xibar1.dat xibar0.dat
