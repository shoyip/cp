---
documentclass: article
classoption: a4paper

numbersections: true
title: Equazioni di Stato per Stelle di Neutroni in Equilibrio Idrostatico
author: Matteo Bettanin \and Shoichi Yip
affiliation: Università di Trento
header-includes: |
	\usepackage{fvextra}
	\DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
	\usepackage{siunitx}
	\usepackage{mathptmx}
	\usepackage{float}
	\usepackage{graphicx}
abstract: |
	In questa esercitazione ci occuperemo di risolvere numericamente le
	equazioni di stato di stelle di neutroni in equilibrio idrostatico
	(Equazione di Lane-Emden). Per la
	risoluzione numerica delle equazioni differenziali utilizziamo l'algoritmo
	di Runge-Kutta al quart'ordine. Infine stimiamo la massa in unità SI in
	funzione del raggio delle stelle per $n=1.5$.
...

# Introduzione

Partiamo da un modello di stella sferica, trattando i neutroni come un gas
omogeneo di fermioni. Nell'approssimazione $T\approx\SI{0}{\kelvin}$ i fermioni
non interagiscono tra di loro. Dato $N$ il numero massimo di neutroni
$$
N = \frac{V}{(2\pi)^3}\int_0 ^{K_F} d\mathbf{k}
$$
abbiamo che $\rho = \frac{N}{V}$, se supponiamo che la densità sia omogenea. Da questo
ricaviamo che
$$
\rho = \frac{K_F ^3}{3\pi^2}.
$$

Poiché studiamo una stella di neutroni in equilibrio idrostatico, vale
l'equazione di stato
$$
\frac{1}{\rho(r)} \frac{dP}{dr} = - \frac{GM(r)}{r^2}.
$$

Se risolviamo questa equazione per $M(r)$ e la differenziamo possiamo
confrontarla con l'espressione per il guscio sferico infinitesimale $dM =
4\pi r^2 \rho\, dr$ e otteniamo
$$
\frac{1}{r^2} \left( \frac{r^2}{\rho} \frac{dP}{dr} \right) = -4\pi G \rho.
$$

Nell'assunzione politropica $P = k \rho_m^{(1+\frac{1}{n})}$, definite le
grandezze $\rho_m = \rho_c \theta^n$ e $\gamma = (n+1)/n$, con $\rho_c$ la densità al centro della stella, abbiamo che
$$
\left[ \frac{n+1}{4\pi G} k\rho_c^{\frac{1-n}{n}} \right] \frac{1}{r^2}
\frac{d}{dr} \left( r^2 \frac{d\theta}{dr} \right) = -\theta^n.
$$

Se sostituiamo ancora $\xi = r/\alpha$ dove
$$
\alpha = \sqrt{\frac{n+1}{4\pi G} k\rho_c^{\frac{1+n}{n}}}
$$
otteniamo la cosiddetta *equazione di Lane-Emden* per fluidi politropici in
equilibrio idrostatico
$$
\frac{1}{\xi^2} \frac{d}{d\xi} \left( \xi^2 \frac{d\theta}{d\xi} \right) =
-\theta^n
$$
in cui $\theta$ rappresenta la densità adimensionale.

Le condizioni al contorno sono definite dal fatto che:

1. la densità a raggio nullo è la densità al centro $\rho_c$, quindi
   $\theta(0)=1$;
2. poiché il sistema è all'equilibrio al centro, avremo pressione massima per
   $r=0$ e quindi $\frac{d\theta}{d\xi}\rvert_{\xi=0}=0$.

Possiamo riscrivere l'equazione differenziale al second'ordine come un sistema
di equazioni differenziali al prim'ordine
$$
\begin{cases}
\phi(\xi) &= \frac{d\theta}{d\xi} (\xi)\\
\frac{d\phi}{d\xi} (\xi) &= -\frac{2}{\xi} \phi(\xi) - \theta^n(\xi)
\end{cases}.
$$

Una volta risolto il sistema possiamo trovare il valore di $\xi$ per il  quale
$\theta$ si annulla, lo chiamiamo $\bar{\xi}$. Per valori di $\xi > \bar{\xi}$ la funzione $\theta$
assume valori negativi. Poiché $\theta$ rappresenta la densità adimensionale, per valori negativi perde di significato.
Possiamo quindi fissare $R = \alpha \bar{\xi}$ come raggio della stella.


In ultima istanza utilizziamo il valore di $\bar{\xi}$ e $\theta(\bar{\xi})$
ottenuti numericamente per estrarre da
$$
M(R) = -  4 \pi \left( \frac{5k_{3/2}}{8\pi G} \right)^3 \frac{\bar{\xi^5}}{R^3}\phi(\bar{\xi})
$$
il raggio di stelle aventi massa compresa tra 1.5 e 3 masse solari.


# Implementazione, esecuzione ed esiti del programma

Mostriamo di seguito i risultati ottenuti per la funzione $\theta(\xi)$ per
valori di $n$ compresi tra 1.5 e 3.

\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{fig1.pdf}
\end{figure}

Provando a calcolare la funzione per valori di $n$ fuori dall'intervallo, nel
nostro caso 1 e 4, vediamo che i risultati ottenuti non hanno senso, infatti per
$n = 1$ troviamo che la funzione oscilla attorno allo zero, mentre per $n
= 4$ la funzione tende asintoticamente a zero ma non si annulla mai. Questo risultato
non è fisicamente accettabile perché presupporrebbe l'esistenza di stelle dal raggio infinito.

\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{fig2.pdf}
\end{figure}

I valori di $\bar{\xi}$ sono riportati nella seguente tabella:

$n$         |$\bar{\xi}$ |
------------|------------|
      1.5   | 3.64082    |
      1.7   | 3.68082    |
      2.1   | 3.88081	   |
      2.5   | 3.92080    |
      3.0   | 4.50077	   |



Nel grafico successivo sono riportati i valori dei raggi di stelle con massa nel range prima riportato.

\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{massa4.pdf}
\caption{Grafio relativo a $n=1.5$}
\end{figure}

# Listato del programma

Questo è il nostro file `neutstar.cpp` che compila nell'eseguibile `neutstar`:

``` cpp
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#define PI 3.14159265359
#define G 6.67e-11
#define NMAX 10000
#define HBAR 1.05457e-34
#define MSUN 1.9891e30
#define MNEU 1.67492e-27
#define LOG(x) std::cout << x << std::endl;

double h, nval, input_n, wor, phifin, xibar;
void rk4 (double* x, double* y, double* z, double (*f)(double), double (*g)(double, double, double));
double g (double xi, double theta, double phi);
double f (double phi);
int np;
char* filetheta;
std::ofstream outfile;
double k32 = (pow(HBAR, 2.)*pow(3*PI*PI,2./3.))/(5*pow(MNEU,8./3.));

int main (int argc, char* argv[]) {
	if (argc<=1) {
		std::cout << "[ERROR] n value and filename missing, insert n as first argument and filename as second argument" << std::endl;
		exit(1);
	} else if (argc<=2) {
		std::cout << "[ERROR] n value or filename missing, insert n as first argument and filename as second argument" << std::endl;
		exit(1);
	} else {
		input_n = atof(argv[1]);
		filetheta = argv[2];
	}
	np = 1000;
	nval = input_n;
	// set initial conditions and time interval
	double xi0 = 0.001, xif = 20.0, theta0 = 1.0, phi0 = 0.0;
	double xi[np], theta[np], phi[np], mass[np], rad[np];
	xi[0] = xi0;
	theta[0] = theta0;
	phi[0] = phi0;
	h=(xif-xi0)/np;
	// populate vector xi
	outfile.open("data/xi.dat");
	for (int i=0; i<=np; i++) {
		xi[i] = xi0+i*h;
		outfile << std::setprecision(10) << xi[i] << std::endl;
	}
	outfile.close();

	// integrate
	rk4(xi, theta, phi, f, g);

	// write data to file
	outfile.open(filetheta);
	for (int i=5; i<=np; i++) {
		if (isnan(theta[i])==1){
			std::cout << xi[i-2] << "\t" << phi[i-2] << std::endl;
		    xibar = xi[i];
		    phifin = phi[i];
		}
		outfile << std::setprecision(10) << "\t" << theta[i] << std::endl;
	}
	outfile.close();

	wor = (-4*PI*pow(5*k32/(8*PI*G), 3.)*pow(xibar,5.)*phifin);
	outfile.open("filemass.dat");
	for(int i = 2; i<= np; i++){
    mass[i] = 1.5*MSUN + i*(1.5*MSUN/np);
    rad[i] = pow(wor/mass[i], 1./3.);
    outfile << std::setprecision(10)<< rad[i] << "\t" << mass[i] <<  std::endl;
  }
	outfile.close();
	return 0;
}

void rk4 (double* x, double* y, double* z, double (*f)(double), double (*g)(double, double, double))
{
	// rk4 takes array y, array t, slope function, h mesh size and np
	// number of points, and gives array y and array t
	double k1, k2, k3, k4;
	double l1, l2, l3, l4;
	for (int n = 0; n<=np; n++) {
		k1 = h * f(z[n]);
		l1 = h * g(x[n], y[n], z[n]);
		k2 = h * f(z[n] + .5*l1);
		l2 = h * g(x[n] + .5*h, y[n] + .5*k1, z[n] + .5*l1);
		k3 = h * f(z[n] + .5*l2);
		l3 = h * g(x[n] + .5*h, y[n] + .5*k2, z[n] + .5*l2);
		k4 = h * f(z[n] + l3);
		l4 = h * g(x[n] + h, y[n] + k3, z[n] + l3);
		y[n+1] = y[n] + (k1 + 2.*k2 + 2.*k3 + k4)/6.;
		z[n+1] = z[n] + (l1 + 2.*l2 + 2.*l3 + l4)/6.;
	}
}

double f (double phi)
{
	return phi;
}

double g (double xi, double theta, double phi)
{
	double mypow = pow(theta,nval);
	return -2.0/xi*phi-mypow;
}
```

Il programma calcola i valori numerici di $\theta$ e $\phi$ per un solo $n$. Al
fine di trovare le soluzioni dell'equazione di Lane-Emden per una coorte più ampia
di valori di $n$ abbiamo usato il seguente script `neutstar.sh` che itera su determinati valori di $n$
eseguendo il programma `neutstar` e genera le figure viste in precedenza. Il tempo di esecuzione dello script è
\SI{0.669}{\second}.

```bash
#!/bin/bash

# This script runs the program `neutstar` (make sure you have it in your current
# folder!) for different values of n, then makes a plot of them and exports it
# in pdf format. Then cleans data directory.

mkdir data data2
touch xibar.dat

array1=('1.5' '1.7' '2.1' '2.5' '3.0')

for i in "${array1[@]}"; do
	echo -e "$i" >> xibar0.dat
	./neutstar $i data/theta$i.dat >> xibar1.dat;
	paste xibar0.dat xibar1.dat > xibar.dat;
done

paste data/xi.dat data/theta*.dat > data/output.dat;
gnuplot plotscript

array2=('1.0' '4.5')

for i in "${array2[@]}"; do
	./neutstar $i data2/theta$i.dat;
done

paste data/xi.dat data2/theta*.dat > data2/output.dat;
gnuplot plotscript2

rm -Rf data data2 xibar1.dat xibar0.dat

```
