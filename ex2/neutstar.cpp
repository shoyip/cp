#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#define PI 3.14159265359
#define G 6.67e-11
#define NMAX 10000
#define HBAR 1.05457e-34
#define MSUN 1.9891e30
#define MNEU 1.67492e-27
#define LOG(x) std::cout << x << std::endl;

double h, nval, input_n, wor, phifin, xibar;
void rk4 (double* x, double* y, double* z, double (*f)(double), double (*g)(double, double, double));
double g (double xi, double theta, double phi);
double f (double phi);
int np;
char* filetheta;
std::ofstream outfile;
double k32 = (pow(HBAR, 2.)*pow(3*PI*PI,2./3.))/(5*pow(MNEU,8./3.));

int main (int argc, char* argv[]) {
	if (argc<=1) {
		std::cout << "[ERROR] n value and filename missing, insert n as first argument and filename as second argument" << std::endl;
		exit(1);
	} else if (argc<=2) {
		std::cout << "[ERROR] n value or filename missing, insert n as first argument and filename as second argument" << std::endl;
		exit(1);
	} else {
		input_n = atof(argv[1]);
		filetheta = argv[2];
	}
	np = 1000;
	nval = input_n;
	// set initial conditions and time interval
	double xi0 = 0.001, xif = 20.0, theta0 = 1.0, phi0 = 0.0;
	double xi[np], theta[np], phi[np], mass[np], rad[np];
	xi[0] = xi0;
	theta[0] = theta0;
	phi[0] = phi0;
	h=(xif-xi0)/np;
	// populate vector xi
	outfile.open("data/xi.dat");
	for (int i=0; i<=np; i++) {
		xi[i] = xi0+i*h;
		outfile << std::setprecision(10) << xi[i] << std::endl;
	}
	outfile.close();

	// integrate
	rk4(xi, theta, phi, f, g);

	// write data to file
	outfile.open(filetheta);
	for (int i=5; i<=np; i++) {
		if (isnan(theta[i])==1){
			std::cout << xi[i-2] << "\t" << phi[i-2] << std::endl;
		    xibar = xi[i];
		    phifin = phi[i];
		}
		outfile << std::setprecision(10) << "\t" << theta[i] << std::endl;
	}
	outfile.close();

	wor = (-4*PI*pow(5*k32/(8*PI*G), 3.)*pow(xibar,5.)*phifin);
	outfile.open("filemass.dat");
	for(int i = 2; i<= np; i++){
    mass[i] = 1.5*MSUN + i*(1.5*MSUN/np);
    rad[i] = pow(wor/mass[i], 1./3.);
    outfile << std::setprecision(10)<< rad[i] << "\t" << mass[i] <<  std::endl;
  }
	outfile.close();
	return 0;
}

void rk4 (double* x, double* y, double* z, double (*f)(double), double (*g)(double, double, double))
{
	// rk4 takes array y, array t, slope function, h mesh size and np
	// number of points, and gives array y and array t
	double k1, k2, k3, k4;
	double l1, l2, l3, l4;
	for (int n = 0; n<=np; n++) {
		k1 = h * f(z[n]);
		l1 = h * g(x[n], y[n], z[n]);
		k2 = h * f(z[n] + .5*l1);
		l2 = h * g(x[n] + .5*h, y[n] + .5*k1, z[n] + .5*l1);
		k3 = h * f(z[n] + .5*l2);
		l3 = h * g(x[n] + .5*h, y[n] + .5*k2, z[n] + .5*l2);
		k4 = h * f(z[n] + l3);
		l4 = h * g(x[n] + h, y[n] + k3, z[n] + l3);
		y[n+1] = y[n] + (k1 + 2.*k2 + 2.*k3 + k4)/6.;
		z[n+1] = z[n] + (l1 + 2.*l2 + 2.*l3 + l4)/6.;
	}
}

double f (double phi)
{
	return phi;
}

double g (double xi, double theta, double phi)
{
	double mypow = pow(theta,nval);
	return -2.0/xi*phi-mypow;
}
